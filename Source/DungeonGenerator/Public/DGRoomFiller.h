// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "DGCell.h"
#include "DGRoom.h"

struct FDGRoomStruct;
class ADGGenerator;

/**
 * 
 */
class DUNGEONGENERATOR_API DGRoomFiller
{
public:
    // pure virtual function providing interface framework.
    virtual void Run(ADGRoom* RoomToFill);

protected:

    ADGGenerator* Gen;

    ADGRoom* Room;

    UStaticMesh* FloorMesh;

    UStaticMesh* WallMesh;

    float FloorSize;

    int CurrentLamps = 0;

    int MaxLamps = 0;

    bool IsSameCol(int CellA, int CellB);

    bool IsSameRow(int CellA, int CellB);

    int GetCellRow(int Cell);

    int GetCellCol(int Cell);

    int RowAndColToCell(int Row, int Col);

    bool IsEnoughPlaceForObject(int CellStart, int SizeX, int SizeY, TArray<int>& OccupiedCells);

    bool IsIndexInRoom(int Index);

    void MarkWalkwayInDirection(int StartIndex, bool bHorizontal = true, int Increment = 0);

    bool bIsMainDirHorizontal = false;

    void DrawRoomDebugPoints();

    bool IsEntranceFromDirection(int IndexToTest);

    bool IsValidCellAndWalkway(int Cell);

    int RandomlyRoundFloatNumberIfNotZero(float Number);

    bool IsValidCellAndIsRole(int Cell, ECellRoomRole Role);

    void BuildWallAt(EWallPosition WallPos, int WidthStepFromStart, int HeightStepFromStart);

    bool ShouldBuildWallInDirectionToIndex(int Index, int SrcIndex);
    
    void BuildWalls();

    FVector GetPositionOffsetForLamp(AActor* Lamp);

    FVector LampOffset;

	FVector GetScaleForLamp(FVector LampScale);

};
