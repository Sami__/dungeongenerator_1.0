// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "DGRoom.h"
#include "DGCorridor.generated.h"

class ADGGenerator;
class DGCell; 
class UInstancedStaticMeshComponent;

UCLASS()
class DUNGEONGENERATOR_API ADGCorridor : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ADGCorridor();

	UPROPERTY(VisibleAnywhere)
	UInstancedStaticMeshComponent* MeshComp;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	void SpawnWallsComponent();

	void BuildWallAt(EWallPosition WallPos, FVector Location);

	bool ShouldBuildWallInDirectionToIndex(int DirIndex, int CurrIndex);
	
	UInstancedStaticMeshComponent* WallInstMeshComponent;

	UStaticMesh* FloorMesh;

	float FloorSize;

	UStaticMesh* WallMesh;

	ADGGenerator* Gen;

public:

	TArray<ADGRoom*> IntersectingRooms;

	TArray<int> OccupiedCells;

	void Build(int GridIndexStart, int GridIndexEnd);

	void BuildLShape(ADGRoom* RoomA, ADGRoom* RoomB);

	void Init(ADGGenerator* Generator);

	void BuildWalls();
};
