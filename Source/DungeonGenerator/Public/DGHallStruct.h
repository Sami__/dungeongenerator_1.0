// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "DGRoomStruct.h"
#include "DGHallStruct.generated.h"


USTRUCT(BlueprintType)
struct FDGHallStruct : public FDGRoomStruct
{
	GENERATED_BODY()

	FORCEINLINE FDGHallStruct();

	/** Actor used as a Throne */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Throne")
		TSubclassOf<AActor> Throne;

	/** How many cells does Throne take in X axis (this results in how Throne is placed) */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Throne")
		int ThroneSizeX = 3;

	/** How many cells does Throne take in Y axis (this results in how Throne is placed) */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Throne")
		int ThroneSizeY = 2;


	/** Minimal scale for Throne */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Throne", Meta = (ClampMin = "0"))
		float MinThroneScale = 1;

	/** Maximal scale for Throne */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Throne", Meta = (ClampMin = "0"))
		float MaxThroneScale = 3;


	/** Actor used as a Pillar */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Pillar", Meta = (ClampMin = "0"))
		TSubclassOf<AActor> Pillar;

	/** How many cells does Pillar take in X axis (this results in how Throne is placed) */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Pillar", Meta = (ClampMin = "0"))
		int PillarSizeX = 1;

	/** How many cells does Pillar take in Y axis (this results in how Throne is placed) */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Pillar", Meta = (ClampMin = "0"))
		int PillarSizeY = 1;

	/** How many cells can be Pillar randomly offset from originally selected location */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Pillar", Meta = (ClampMin = "0"))
		int PillarPlacingVariation = 0;

	/** How many cells should be between Pillars */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Pillar", Meta = (ClampMin = "0"))
		int PillarsSpan = 2;

	/** Minimal width scale for Pillar */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Pillar", Meta = (ClampMin = "0"))
		float MinPillarWidthScale = 3;

	/** Maximal width scale for Pillar */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Pillar", Meta = (ClampMin = "0"))
		float MaxPillarWidthScale = 3;

	/** Minimal height scale for Pillar */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Pillar", Meta = (ClampMin = "0"))
		float MinPillarHeightScale = 3;

	/** Maximal height scale for Pillar */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Pillar", Meta = (ClampMin = "0"))
		float MaxPillarHeightScale = 3;


	/** How many cells can be Pillar randomly offset from originally selected location */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Lamp", Meta = (ClampMin = "0"))
		int LampRandSpan = 0;
};


FORCEINLINE FDGHallStruct::FDGHallStruct()
{
}