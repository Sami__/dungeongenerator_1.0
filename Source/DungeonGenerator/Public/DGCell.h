// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "DGCell.generated.h"

UENUM()
enum ECellType
{
	Empty,
	CRoom,
	Corridor,
};

UENUM()
enum ECellRoomRole
{
	Free,
	HorizontalWalkway,
	VerticalWalkway,
	WalkwayWithEnemy,
	Furniture,
	Entrance,
};

/**
 * 
 */
class DUNGEONGENERATOR_API DGCell
{
public:
	DGCell(ECellType CellType, FVector CellPosition, int CellIndex, AActor* CellActor);

	~DGCell();

	ECellType Type;

	ECellRoomRole Role = ECellRoomRole::Free;

	AActor* OccupiedActor;

	FVector Position;

	bool bIsCorridorFirst = false;
	
	bool bIsCorridorLast = false;

	int Index;

	int RoomIndex;

	bool IsWalkway();
};