// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "DGRoomFiller.h"

struct FDGHallStruct;


/**
 * 
 */
class DUNGEONGENERATOR_API DGHallFiller : public DGRoomFiller
{
public:
	virtual void Run(ADGRoom* RoomToFill) override;

protected:
	FDGHallStruct* HallStruct;

	void BuildPillars();

	void BuildCornerLamps();

	void PlacePillar(int Idx);

	void PlaceLamp(int Idx);

	bool PickSpotForLampAroundPillar(int PillarIndex, int& LampIndex);

	void PlaceThrone();

	FVector PillarOffset;

	FRotator GetRotationForThrone(int SpawnCell);

	FVector GetPositionForThrone(FRotator RotatedThrone);

	FVector GetPositionOffsetForPillar(AActor* Pillar);

	FVector GetScaleForPillar(FVector PillarScale);

	FVector GetScaleForThrone(FVector ThroneScale);
};
