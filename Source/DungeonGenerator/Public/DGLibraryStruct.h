// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "DGRoomStruct.h"
#include "DGLibraryStruct.generated.h"


USTRUCT(BlueprintType)
struct FDGLibraryStruct : public FDGRoomStruct
{
	GENERATED_BODY()

	FORCEINLINE FDGLibraryStruct();


	/** Actor used as a Throne */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Library")
		TSubclassOf<AActor> Library;

	/** How many cells does Library take in X axis (this results in how Throne is placed) */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Library")
		int LibraryTileSizeX = 2;

	/** How many cells does Library take in Y axis (this results in how Throne is placed) */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Library")
		int LibraryTileSizeY = 1;
	
	/** Minimal width scale for Library */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Library", Meta = (ClampMin="0"))
		float MinLibraryScale = 0.7;
	
	/** Maximal width scale for Library */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Library", Meta = (ClampMin="0"))
		float MaxLibraryScale = 0.9;

	/** Maximal angle of which can be Library randomly rotated */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Library", Meta = (ClampMin="0", ClampMax="360"))
		float MaxLibraryRotation = 5;
	
	/** Chance to spawn library on selected position */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Library", Meta = (ClampMin="0", ClampMax="100"))
		int ChanceToSpawnLibrary = 100;

	/** Minimal ratio of number of walkways to number of rows/columns (depends of the orientation of the room) */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Library", Meta = (ClampMin="0", ClampMax="1"))
		float MinWalkwayRatio = 0.2;

	/** Maximal ratio of number of walkways to number of rows/columns (depends of the orientation of the room) */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Library", Meta = (ClampMin = "0", ClampMax = "1"))
		float MaxWalkwayRatio = 1;
};

FORCEINLINE FDGLibraryStruct::FDGLibraryStruct()
{
}