// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "DGRoom.generated.h"

UENUM()
enum ERoomType
{
	Normal,
	Main,
	Unused,
};

UENUM()
enum EWallPosition
{
	Up,
	Down,
	Left,
	Right,
};

class ADGGenerator;
class DGCell;
class DGRoomFiller;
struct FDGRoomStruct;
class UInstancedStaticMeshComponent;

	UCLASS()
class DUNGEONGENERATOR_API ADGRoom : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ADGRoom();

	UPROPERTY(VisibleAnywhere)
	UStaticMeshComponent* MeshComp;
	
	

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	int GridStartIndex;

	FVector LastPosition;

	FVector PreLastPosition;

	int TimesAtTheSamePosition = 0;


	DGRoomFiller* RoomFiller;

	void SelectRoomFiller();
	
	
public:	

	UInstancedStaticMeshComponent* WallInstMeshComponent;

	UInstancedStaticMeshComponent* FloorInstMeshComponent;

	int Height;

	int Width;

	float FloorSize;

	FVector GetCenter();

	TEnumAsByte<ERoomType> RoomType;

	void BecomeTiles();

	void Init(ADGGenerator* Generator);

	void SetGridStartIndex(int index);

	int GetGridStartIndex();
	
	void DestroyAndCleanUp();


	bool HandlePositionCheck();

	bool bIsInvalidated = false;

	TArray<DGCell*> RoomTiles;
	
	void Equip();

	ADGGenerator* Gen;


	FDGRoomStruct* RoomStruct;

	void SpawnWallsComponent(UStaticMesh* WallMesh);

	void ReclaimTilesIfNeeded();

};
