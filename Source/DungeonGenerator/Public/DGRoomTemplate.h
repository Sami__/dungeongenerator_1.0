// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "DGRoomTemplate.generated.h"


USTRUCT()
struct FDGRoomTemplate
{
	GENERATED_BODY()

		FORCEINLINE FDGRoomTemplate();

	UPROPERTY(EditAnywhere)
		UStaticMesh* WallMesh;

	UPROPERTY(EditAnywhere)
		UStaticMesh* FloorMesh;
};

FORCEINLINE FDGRoomTemplate::FDGRoomTemplate()
{
}