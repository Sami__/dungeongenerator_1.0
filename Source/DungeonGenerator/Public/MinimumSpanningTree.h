// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "MinimumSpanningTree.generated.h"

/**
 * 
 */
class DUNGEONGENERATOR_API MinimumSpanningTree
{

public:
	MinimumSpanningTree();

	static FDGGraph ConvertGraphToMST(FDGGraph Original);
};

USTRUCT()
struct FDGVertex
{
	GENERATED_BODY()

	FORCEINLINE FDGVertex();

	explicit FORCEINLINE FDGVertex(FVector Position);

	FVector Position;

	bool operator==(const FDGVertex& V) const;

	bool operator!=(const FDGVertex& V) const;

};


FORCEINLINE FDGVertex::FDGVertex()
{
}

FORCEINLINE FDGVertex::FDGVertex(FVector Position) : Position(Position)
{
}

FORCEINLINE bool FDGVertex::operator==(const FDGVertex& V) const
{
	return Position.Equals(V.Position);
}

FORCEINLINE bool FDGVertex::operator!=(const FDGVertex& V) const
{
	return !Position.Equals(V.Position);
}

USTRUCT()
struct FDGEdge
{
	GENERATED_BODY()

	FORCEINLINE FDGEdge();

	explicit FORCEINLINE FDGEdge(FDGVertex V1, FDGVertex V2, float Length);

	FDGVertex V1;

	FDGVertex V2;

	float Length;

	bool operator==(const FDGEdge& E) const;

	bool operator!=(const FDGEdge& E) const;

};


FORCEINLINE FDGEdge::FDGEdge()
{
}


FORCEINLINE FDGEdge::FDGEdge(FDGVertex V1, FDGVertex V2, float Length) : V1(V1), V2(V2), Length(Length)
{
}


FORCEINLINE bool FDGEdge::operator==(const FDGEdge& E) const
{
	return (V1 == E.V1 && V2 == E.V2) || (V1 == E.V2 && V2 == E.V1);
}

FORCEINLINE bool FDGEdge::operator!=(const FDGEdge& E) const
{
	return (V1 != E.V1 || V2 != E.V2) && (V1 != E.V2 || V2 != E.V1);
}



USTRUCT()
struct FDGGraph
{
	GENERATED_BODY()

	TArray<FDGVertex> Vertices;

	TArray<FDGEdge> Edges;

	FORCEINLINE FDGGraph();

	explicit FORCEINLINE FDGGraph(TArray<FDGVertex> Vertices, TArray<FDGEdge> Edges);
};


FORCEINLINE FDGGraph::FDGGraph()
{
}


FORCEINLINE FDGGraph::FDGGraph(TArray<FDGVertex> Vertices, TArray<FDGEdge> Edges) : Vertices(Vertices), Edges(Edges)
{
}
