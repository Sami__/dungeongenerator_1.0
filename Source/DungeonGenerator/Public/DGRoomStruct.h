// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "DGRoomStruct.generated.h"

class AActor;
/**
 * 
 */
USTRUCT(BlueprintType)
struct FDGRoomStruct
{
	GENERATED_BODY()

	FORCEINLINE FDGRoomStruct();

	/** If set, rooms will not be filled with any meshes nor use any properties from this specific room type */
	UPROPERTY(EditAnywhere, Meta = (DisplayName = "IGNORE THIS ROOM TYPE"))
		bool bIgnoreThisType = false;

	/** Floor mesh to use for this room type. Keep in mind all floor meshes must be the same for rooms and corridors */
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		UStaticMesh* FloorMesh;

	/** Wall mesh to use for this room type */
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		UStaticMesh* WallMesh;

	/** Actor used as a Lamp */
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		TSubclassOf<AActor> Lamp;

	/** How many cells does Lamp take in X axis (this results in how Lamps are placed) */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Lamp", Meta = (ClampMin = "0"))
		int LampSizeX = 1;

	/** How many cells does Lamp take in Y axis (this results in how Lamps are placed) */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Lamp", Meta = (ClampMin = "0"))
		int LampSizeY = 1;

	/** Minimal scale for Lamp */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Lamp", Meta = (ClampMin = "0"))
		float MinLampScale = 0.8;

	/** Maximal scale for Lamp */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Lamp", Meta = (ClampMin = "0"))
		float MaxLampScale = 1;


	/** Minimal number of Lamps per this room type */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Meta = (ClampMin="0"))
		int MinLampsPerRoom = 1;

	/** Maximal number of Lamps per this room type */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Meta = (ClampMin="0"))
		int MaxLampsPerRoom = 5;
};

FORCEINLINE FDGRoomStruct::FDGRoomStruct()
{
}