// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "DGRoomFiller.h"

struct FDGLibraryStruct;
class ADGRoom;

/**
 * 
 */
class DUNGEONGENERATOR_API DGLibraryFiller : public DGRoomFiller
{
public:

	virtual void Run(ADGRoom* RoomToFill) override;


protected:

	void BuildEntranceWalkways();

	FDGLibraryStruct* LibraryStruct;


	int SideDirWalkways = 0;

	void BuildMainDirWalkways();

	void BuildSideDirWalkways();
	
	void PlaceLibraries();

	FRotator GetRotationForLibrary(int CellStart, TArray<bool> FlipAtRowOrColPredefined);

	FVector GetPositionForLibrary(FRotator Rotation, int BaseCell, float Scale);

	float GetSizeForLibrary();
	
	void PlaceLamps();

	void PlaceLamp(int Idx);
	
	bool IsCellViableForLamp(int Cell);

};
