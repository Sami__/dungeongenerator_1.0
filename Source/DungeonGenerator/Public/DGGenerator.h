// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "DGLibraryStruct.h"
#include "DGHallStruct.h"
#include "DGCell.h"
#include "DGGenerator.generated.h"

DECLARE_LOG_CATEGORY_EXTERN(DungeonGenerator, Log, All);



//struct FDGRoomTemplate;
class ADGRoom;
class ADGCorridor;
class ADGCorridor;
struct FDGGraph;

UCLASS()
class DUNGEONGENERATOR_API ADGGenerator : public AActor
{
	DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FBeforeGenerationStarts, int, Seed);
	DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FAfterGenerationEnds, int, Seed);

	GENERATED_BODY()

public:
	// Sets default values for this actor's properties
	ADGGenerator();

	UPROPERTY(BlueprintAssignable, Category = "Events")
	FBeforeGenerationStarts OnBeforeGenerationStarts;

	UPROPERTY(BlueprintAssignable, Category = "Events")
	FAfterGenerationEnds OnAfterGenerationEnds;

	/** Determines how far away will rooms initially spawn - the further away the less collisions and thus less separations will happen afterwards */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "General", Meta = (ClampMin = 0, DisplayName = "Grid Size For Init"))
	int CircleRadius = 5;

	/** Will custom seed be used? */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "General")
	bool bCustomSeed = false;

	/** Value of custom seed */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, BlueprintReadWrite, Category = "General", Meta = (EditCondition = "bCustomSeed"))
	int Seed = 20975;

	/** Number of positions room moves in single step when separation is happening */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "General", Meta = (ClampMin = "0", DisplayName = "Separation Speed Multiplier"))
	int SeparationSpeed = 1;

	/** Minimal width (number of cells) of room */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Rooms", Meta = (ClampMin = "3", ClampMax = "100"))
	int MinRoomWidth = 4;

	/** Maximal width (number of cells) of room */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Rooms", Meta = (ClampMin = "3", ClampMax = "100"))
	int MaxRoomWidth = 8;

	/** Minimal height (number of cells) of room */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Rooms", Meta = (ClampMin = "3", ClampMax = "100"))
	int MinRoomHeight = 4;

	/** Minimal height (number of cells) of room */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Rooms", Meta = (ClampMin = "3", ClampMax = "100"))
	int MaxRoomHeight = 8;

	/** Number of rooms to spawn. Some rooms will be deleted during generation process because they won't be connected at all (nor selected as Main rooms), so the final number of rooms will be lower */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Rooms", Meta = (ClampMin = "3", ClampMax = "10000"))
	int RoomsCount = 100;

	/** What should be the minimal volume ratio of room to become Main room. Main rooms will connect to each other with corridors */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Rooms", Meta = (ClampMin = "0", DisplayName = "Main Room Selection Ratio"))
	float MainRoomMultiplier = 1.3;

	/** Larger number = more corridors. Main room connections are first created using Delaunay Triangulation and from that only minimal path is selected. This property specifies what ratio of removed corridors should be restored */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Meta = (ClampMin = "0", ClampMax = "100", DisplayName = "Delaunay Linkage Ratio"), Category = "Rooms")
	float RoomsLinkage = 0;

	/** If unchecked, rooms will not spawn walls even if all wall meshes are set */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Rooms")
	bool bSpawnRoomWalls = true;

	
	/** If unchecked, corridors will not spawn walls even if all wall meshes are set */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Corridors")
	bool bSpawnCorridorWalls = true;

	/** If checked, walls will be built when corridor and room are right next to each other. Unchecked can result in more open spaces */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Corridors")
	bool bBuildWallsAroundRooms = false;

	/** Floor mesh to use for corridor. Keep in mind all floor meshes must be the same for rooms and corridors */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Corridors")
	UStaticMesh* CorridorFloorMesh;

	/** Wall mesh to use for corridor */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Corridors")
	UStaticMesh* CorridorWallMesh;

	/** If checked, rooms will be filled with objects from Room Templates category */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Room Templates")
	bool bWantToEquipRooms = true;

	/** Defines properties for room of type Library */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Room Templates", Meta = (DisplayName = "Library"))
	FDGLibraryStruct LibraryStruct;

	/** Defines properties for room of type Hall */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Room Templates", Meta = (DisplayName = "Hall"))
	FDGHallStruct HallStruct;

	TArray<FDGRoomStruct*> RoomStructs;


protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	FRandomStream Stream;

	TArray<ADGRoom*> SpawnRooms(TArray<FVector> Locations);

	TArray<ADGCorridor*> SpawnCorridors(FDGGraph MST);

	TArray<ADGRoom*> SelectMainRooms();

	void SeparateRooms();

	float GetAverageRoomSize();

	TArray<ADGRoom*> SpawnedRooms;

	TArray<ADGRoom*> MainRooms;

	TArray<ADGRoom*> DestroyedRooms;

	TArray<ADGCorridor*> SpawnedCorridors;

	bool GetRowIndexesIfRoomsOverlap(ADGRoom* RoomA, ADGRoom* RoomB, int& StartIndex, int& EndIndex);

	bool GetColumnIndexesIfRoomsOverlap(ADGRoom* RoomA, ADGRoom* RoomB, int& StartIndex, int& EndIndex);

	FDGGraph CreateDelaunayGraph();

	void FillTileGrid();

	void AddSpawnedRoomsToTileGrid(FVector2D MinimalGridPosition);

	void RenewSomeRoomsLinkage(FDGGraph OriginalDelaunayGraph, FDGGraph *MinimalSpanningTree);

	DGCell GetCellAtPosition(int X, int Y);

	void SetCellAtPosition(int X, int Y, DGCell Cell);
	
	TArray<DGCell> TileGrid;

	int TileGridWidth;
	
	int TileGridHeight;

	void PrintDebugCells();

	FTimerHandle RoomTimer;

	TArray<FVector> RoomLocations;

	int LocationsIndex;

	void SpawnRoom();

	void RemoveUnusedRooms();

	bool CheckParameters();

	void SpawnWalls();

	float FloorSize;

	void EquipRooms();

	void FillRoomStructs();

public:	

	static const FName DUNGEON_TAG;

	TArray<FVector> GetRandomPointsInGrid(FVector Origin, int Radius, int Count);

	bool bDebug = false;
	
	/** Let's generate! */
	UFUNCTION(BlueprintCallable, CallInEditor, Category = "Actions!")
	void Generate();

	/** Let's ungenerate ! */
	UFUNCTION(BlueprintCallable, CallInEditor, Category = "Actions!")
	void Clear();


	int IdxToCol(int Index);

	int IdxToRow(int Index);

	int GetTileGridWidth();

	TArray<DGCell>* GetTileGrid();
	
	float GetSizeOfFloorMesh(UStaticMesh* Mesh);

	float GetFloorSize();

	FRandomStream* GetCurrentStream();

	bool IsActorForObjectSet(TSubclassOf<AActor> CheckedObject, FString CheckedObjectName, bool bWithMessage = false);


};

