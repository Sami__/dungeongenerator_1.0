// Fill out your copyright notice in the Description page of Project Settings.


#include "MinimumSpanningTree.h"

#include "Math/Vector.h"

MinimumSpanningTree::MinimumSpanningTree()
{
}



FDGGraph MinimumSpanningTree::ConvertGraphToMST(FDGGraph Original)
{

	FDGGraph ResultGraph;

	if (Original.Vertices.Num() < 1)
	{
		return ResultGraph;
	}

	ResultGraph.Vertices.Add(Original.Vertices[0]);

	while (ResultGraph.Vertices.Num() < Original.Vertices.Num())
	{
		TArray<FDGEdge> EdgeOptions;
		
		for (FDGVertex Vert : ResultGraph.Vertices)
		{
			EdgeOptions.Append(Original.Edges.FilterByPredicate([Vert, ResultGraph](FDGEdge InEdge) {
				return (InEdge.V1 == Vert || InEdge.V2 == Vert) && !ResultGraph.Edges.Contains(InEdge);
			}));
		}
		

		FDGEdge Lowest;
		FDGVertex ToPush;
		bool bDefined = false;

		for (FDGEdge E : EdgeOptions)
		{
			if (!bDefined || E.Length < Lowest.Length)
			{

				if (ResultGraph.Vertices.Contains(E.V1) ^ ResultGraph.Vertices.Contains(E.V2)) 
				{
					Lowest = E;
					bDefined = true;

					if (ResultGraph.Vertices.Contains(E.V1))
					{
						ToPush = E.V2;
					}
					else
					{
						ToPush = E.V1;
					}

				}
			}
		}

		ResultGraph.Edges.Push(Lowest);
		ResultGraph.Vertices.Push(ToPush);
	}

	return ResultGraph;
}


