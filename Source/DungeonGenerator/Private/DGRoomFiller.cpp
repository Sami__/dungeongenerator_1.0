// Fill out your copyright notice in the Description page of Project Settings.


#include "DGRoomFiller.h"
#include "DGRoom.h"
#include "DGCell.h"
#include "DGGenerator.h"
#include "DGCorridor.h"
#include "DrawDebugHelpers.h"
#include "Engine/StaticMesh.h" 


void DGRoomFiller::Run(ADGRoom* RoomToFill)
{
	Room = RoomToFill;
	Gen = Room->Gen;
	bIsMainDirHorizontal = (bool)Gen->GetCurrentStream()->RandRange(0, 1);
	FloorSize = Gen->GetFloorSize();
	WallMesh = Room->RoomStruct->WallMesh;
	MaxLamps = Gen->GetCurrentStream()->RandRange(Room->RoomStruct->MinLampsPerRoom, Room->RoomStruct->MaxLampsPerRoom);
	LampOffset = FVector(0);
	
	if (Room->Gen->bSpawnRoomWalls)
	{
		BuildWalls();
	}
}


bool DGRoomFiller::IsSameCol(int CellA, int CellB)
{
	return (Room->RoomTiles.IsValidIndex(CellA) && Room->RoomTiles.IsValidIndex(CellB)) && GetCellCol(CellA) == GetCellCol(CellB);
}


bool DGRoomFiller::IsSameRow(int CellA, int CellB)
{
	return (Room->RoomTiles.IsValidIndex(CellA) && Room->RoomTiles.IsValidIndex(CellB)) && (GetCellRow(CellA) == GetCellRow(CellB));
}


int DGRoomFiller::GetCellRow(int Cell)
{
	return Cell % Room->Height;
}


int DGRoomFiller::GetCellCol(int Cell)
{
	return Cell / Room->Height;
}


bool DGRoomFiller::IsIndexInRoom(int Index)
{
	return (*Gen->GetTileGrid()).IsValidIndex(Index) && (*Gen->GetTileGrid())[Index].OccupiedActor == Room;
}


bool DGRoomFiller::IsValidCellAndWalkway(int Cell)
{
	return Room->RoomTiles.IsValidIndex(Cell) && Room->RoomTiles[Cell]->IsWalkway();
}


bool DGRoomFiller::IsValidCellAndIsRole(int Cell, ECellRoomRole Role)
{
	return Room->RoomTiles.IsValidIndex(Cell) && Room->RoomTiles[Cell]->Role == Role;
}


bool DGRoomFiller::IsEnoughPlaceForObject(int CellStart, int SizeX, int SizeY, TArray<int>& OccupiedCells)
{
	int Row = GetCellRow(CellStart);

	for (int i = 0; i < SizeX; i++)
	{
		int CellIdx = CellStart + i * Room->Height;
		int XColumn = GetCellCol(CellIdx);

		if (Row != GetCellRow(CellIdx))
		{
			return false;
		}

		for (int j = 0; j < SizeY; j++)
		{
			int CellIdxY = CellIdx + j;

			if (!Room->RoomTiles.IsValidIndex(CellIdxY)
				|| Room->RoomTiles[CellIdxY]->Role != ECellRoomRole::Free
				|| XColumn != GetCellCol(CellIdxY))
			{
				return false;
			}

			OccupiedCells.Add(CellIdxY);
		}

	}

	return true;
}


void DGRoomFiller::MarkWalkwayInDirection(int StartIndex, bool bHorizontal, int Increment)
{
	FColor DbgCol;

	if (Increment == 0)
	{
		if (bHorizontal)
		{
			DbgCol = FColor::Blue;
			Increment = !IsIndexInRoom(StartIndex + 1) ? -1 : 1;
		}
		else
		{
			int TGW = Gen->GetTileGridWidth();
			Increment = !IsIndexInRoom(StartIndex + TGW) ? -TGW : TGW;
			DbgCol = FColor::Green;
		}
	}
	else
	{
		DbgCol = FColor(100, 0, 130);
	}

	while (true)
	{
		DGCell* C = &(*Gen->GetTileGrid())[StartIndex];
		
		if (C->Role != ECellRoomRole::Entrance)
		{
			C->Role = bHorizontal ? ECellRoomRole::HorizontalWalkway : ECellRoomRole::VerticalWalkway;
		}


		StartIndex += Increment;

		if (!IsIndexInRoom(StartIndex))
		{
			break;
		}
	}
}


void DGRoomFiller::DrawRoomDebugPoints()
{
	for (DGCell* Tile : Room->RoomTiles)
	{
		switch (Tile->Role)
		{
		case ECellRoomRole::HorizontalWalkway:
		case ECellRoomRole::VerticalWalkway:
		{
			DrawDebugPoint(Room->GetWorld(), Tile->Position + FVector(0, 0, 200.f), 30.f, FColor::Blue, true);
		}
		break;

		case ECellRoomRole::Entrance:
		{
			DrawDebugPoint(Room->GetWorld(), Tile->Position + FVector(0, 0, 160.f), 30.f, FColor::Green, true);
		}
		break;

		case ECellRoomRole::Furniture:
		{
			DrawDebugPoint(Room->GetWorld(), Tile->Position + FVector(0, 0, 120.f), 30.f, FColor::Purple, true);
		}
		break;

		case ECellRoomRole::WalkwayWithEnemy:
		{
			DrawDebugPoint(Room->GetWorld(), Tile->Position + FVector(0, 0, 80.f), 30.f, FColor::Orange, true);
		}
		break;

		case ECellRoomRole::Free:
		{
			DrawDebugPoint(Room->GetWorld(), Tile->Position + FVector(0, 0, 40.f), 30.f, FColor::Silver, true);
		}
		break;
		default:
		{
			DrawDebugPoint(Room->GetWorld(), Tile->Position + FVector(0, 0, 0.f), 30.f, FColor::Red, true);
		}
		break;
		}
	}
}


bool DGRoomFiller::IsEntranceFromDirection(int IndexToTest)
{
	if (!(*Gen->GetTileGrid()).IsValidIndex(IndexToTest))
	{
		return false;
	}

	DGCell Cell = (*Gen->GetTileGrid())[IndexToTest];

	if (Cell.Type != ECellType::Corridor)
	{
		return false;
	}

	if (Cell.bIsCorridorFirst || Cell.bIsCorridorLast)
	{
		return true;
	}

	if (Cast<ADGCorridor>(Cell.OccupiedActor)->IntersectingRooms.Contains(Room))
	{
		return true;
	}

	return false;
}


int DGRoomFiller::RowAndColToCell(int Row, int Col)
{
	return Col * Room->Height + Row;
}


int DGRoomFiller::RandomlyRoundFloatNumberIfNotZero(float Number)
{
	if (FMath::IsNearlyEqual(Number, 0))
	{
		return 0;
	}


	if ((bool)Gen->GetCurrentStream()->RandRange(0, 1))
	{
		return FMath::FloorToInt(Number);
	}
	else
	{
		return FMath::CeilToInt(Number);
	}
}



void DGRoomFiller::BuildWalls()
{

	if (!WallMesh)
	{
		return;
	}



	Room->SpawnWallsComponent(WallMesh);

	int GridWidth = Gen->GetTileGridWidth();

	for (int w = 0; w < Room->Width; w++)
	{
		for (int h = 0; h < Room->Height; h++)
		{


			// checking above the index where we want to build the wall
			int CurrIdx = Room->GetGridStartIndex() + (GridWidth * h) + w;
			int CurrCol = Gen->IdxToCol(CurrIdx);

			// are we in the grid?


			if (ShouldBuildWallInDirectionToIndex(CurrIdx - GridWidth, CurrIdx))
			{
				BuildWallAt(EWallPosition::Up, w, h);
			}

			if (ShouldBuildWallInDirectionToIndex(CurrIdx + GridWidth, CurrIdx))
			{
				BuildWallAt(EWallPosition::Down, w, h);
			}

			if ((CurrCol - 1) != Gen->IdxToCol(CurrIdx - 1) || ShouldBuildWallInDirectionToIndex(CurrIdx - 1, CurrIdx))
			{
				BuildWallAt(EWallPosition::Left, w, h);
			}

			if ((CurrCol + 1) != Gen->IdxToCol(CurrIdx + 1) || ShouldBuildWallInDirectionToIndex(CurrIdx + 1, CurrIdx))
			{
				BuildWallAt(EWallPosition::Right, w, h);
			}
		}
	}

}


bool DGRoomFiller::ShouldBuildWallInDirectionToIndex(int Index, int SrcIndex)
{

	if (Index < 0 || Index >((*Gen->GetTileGrid()).Num() - 1))
	{
		return true;
	}
	
	FVector Pos = (*Gen->GetTileGrid())[Index].Position + FVector(0, 0, 40.f);
	   
	DGCell Cell = (*Gen->GetTileGrid())[Index];

	if (Cell.Type == ECellType::Empty)
	{
		return true;
	}
	else if (Cell.Type == ECellType::Corridor)
	{
		if ((Cell.bIsCorridorFirst || Cell.bIsCorridorLast) || (Cast<ADGCorridor>(Cell.OccupiedActor)->IntersectingRooms.Contains(Room)))
		{
			(*Gen->GetTileGrid())[SrcIndex].Role = ECellRoomRole::Entrance;
			return false;
		}


		if (!Room->Gen->bBuildWallsAroundRooms)
		{
			return false;
		}		

		return true;

	}


	return false;
}


void DGRoomFiller::BuildWallAt(EWallPosition WallPos, int WidthStepFromStart, int HeightStepFromStart)
{

	FRotator Rot;
	FVector Loc(0, 0, 0);

	Loc.X = WidthStepFromStart * FloorSize;
	Loc.Y = HeightStepFromStart * FloorSize;

	switch (WallPos)
	{
	case EWallPosition::Up:
	{
		Rot = FRotator(0, 0, 0);	
		break;

	}
	case EWallPosition::Down:
	{
		Rot = FRotator(0, 0, 0);
		Loc.Y += FloorSize - WallMesh->GetBoundingBox().GetExtent().Y * 2;
		break;

	}
	case EWallPosition::Left:
	{
		Rot = FRotator(0, 90, 0);
		Loc.X += WallMesh->GetBoundingBox().GetExtent().Y * 2;

		break;
	}
	case EWallPosition::Right:
	{
		Rot = FRotator(0, 90, 0);
		Loc.X += FloorSize;
		break;

	}

	}


	Room->WallInstMeshComponent->AddInstance(FTransform(Rot, Loc));
}


FVector DGRoomFiller::GetPositionOffsetForLamp(AActor* Lamp)
{
	if (LampOffset.Equals(FVector(0)))
	{
		FVector Origin;
		FVector BoxExtent;
		Lamp->GetActorBounds(false, Origin, BoxExtent, false);

		LampOffset = FVector((Room->FloorSize / 2) - BoxExtent.X / 2, (Room->FloorSize / 2) - BoxExtent.Y / 2, 0);
	}

	return LampOffset;
}


FVector DGRoomFiller::GetScaleForLamp(FVector LampScale)
{
	float W = Gen->GetCurrentStream()->FRandRange(Room->RoomStruct->MinLampScale, Room->RoomStruct->MaxLampScale);
	return LampScale * W;
}