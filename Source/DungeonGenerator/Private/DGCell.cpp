

#include "DGCell.h"

DGCell::DGCell(ECellType CellType, FVector CellPosition, int CellIndex, AActor* CellActor = nullptr)
{
	Type = CellType;
	OccupiedActor = CellActor;
	Position = CellPosition;
	Index = CellIndex;
}

DGCell::~DGCell()
{
}


bool DGCell::IsWalkway()
{
	return Role == ECellRoomRole::HorizontalWalkway || Role == ECellRoomRole::VerticalWalkway;
}
