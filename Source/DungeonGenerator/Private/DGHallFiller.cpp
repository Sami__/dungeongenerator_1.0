// Fill out your copyright notice in the Description page of Project Settings.


#include "DGHallFiller.h"
#include "DGLibraryStruct.h"
#include "DGRoom.h"
#include "DGCell.h"
#include "DGGenerator.h"
#include "DGCorridor.h"
#include "DrawDebugHelpers.h"


void DGHallFiller::Run(ADGRoom* RoomToFill)
{
	DGRoomFiller::Run(RoomToFill);

	if (!Gen->bWantToEquipRooms)
	{
		return;
	}

	PillarOffset = FVector(0);
	HallStruct = &Gen->HallStruct;
	
	if (Room->Gen->IsActorForObjectSet(HallStruct->Lamp, FString("Lamp")))
	{
		BuildCornerLamps();
	}
	
	if (Room->Gen->IsActorForObjectSet(HallStruct->Lamp, FString("Pillars")))
	{
		BuildPillars();
	}

	if (Room->Gen->IsActorForObjectSet(HallStruct->Lamp, FString("Throne")))
	{
		PlaceThrone();
	}

}


void DGHallFiller::BuildPillars()
{
	int HorizontalNum = Room->Width / HallStruct->PillarsSpan;
	int VerticalNum = Room->Height / HallStruct->PillarsSpan;

	float FirstHorIdxRem = ((float)((HallStruct->PillarsSpan - 1) + (Room->Width % HallStruct->PillarsSpan)) / 2);
	float FirstVertIdxRem = ((float)((HallStruct->PillarsSpan - 1) + (Room->Height % HallStruct->PillarsSpan)) / 2);
	
	int FirstHorIdx = (int)FirstHorIdxRem;
	int FirstVertIdx = (int)FirstVertIdxRem;

	FirstHorIdxRem -= FirstHorIdx;
	FirstVertIdxRem -= FirstVertIdx;

	FirstHorIdx += RandomlyRoundFloatNumberIfNotZero(FirstHorIdxRem);
	FirstVertIdx += RandomlyRoundFloatNumberIfNotZero(FirstVertIdxRem);

	for (int h = 0; h < HorizontalNum; h++)
	{
		int HorIdx = FirstHorIdx + (h * HallStruct->PillarsSpan);
		
		for (int v = 0; v < VerticalNum; v++)
		{
			int VertIdx = FirstVertIdx + (v * HallStruct->PillarsSpan);
			HorIdx += Gen->GetCurrentStream()->RandRange(-HallStruct->PillarPlacingVariation, HallStruct->PillarPlacingVariation);
			VertIdx += Gen->GetCurrentStream()->RandRange(-HallStruct->PillarPlacingVariation, HallStruct->PillarPlacingVariation);
			int PillarCell = RowAndColToCell(VertIdx, HorIdx);
			PlacePillar(PillarCell); 
		}
	}

}


void DGHallFiller::PlacePillar(int Idx)
{

	if (!Room->RoomTiles.IsValidIndex(Idx))
	{
		return;
	}
	
	DGCell* C = Room->RoomTiles[Idx];

	if (C->Role == ECellRoomRole::Entrance)
	{
		return;
	}

	C->Role = ECellRoomRole::Furniture;
	AActor* Pillar = Gen->GetWorld()->SpawnActor<AActor>(HallStruct->Pillar, C->Position, FRotator::ZeroRotator);
	Pillar->SetActorScale3D(GetScaleForPillar(Pillar->GetActorScale3D()));
	Pillar->Tags.Add(ADGGenerator::DUNGEON_TAG);

	FVector PillarPos = C->Position + GetPositionOffsetForPillar(Pillar);
	Pillar->SetActorLocation(PillarPos);
	
	int LampSpot;
	if (PickSpotForLampAroundPillar(Idx, LampSpot))
	{
		PlaceLamp(LampSpot);
	}

}


void DGHallFiller::BuildCornerLamps()
{

	int RandSpan = HallStruct->LampRandSpan + 1;

	TArray<int> Corners = {0, Room->Height * (Room->Width - RandSpan), Room->Height - RandSpan, Room->Height * (Room->Width - RandSpan) + Room->Height - RandSpan};
	
	auto lambda = [this](int A, int B) { return (bool)Room->Gen->GetCurrentStream()->RandRange(0, 1); };

	Corners.Sort(lambda);

	for (int i = 0; i < Corners.Num(); i++)
	{
		PlaceLamp(Corners[i]);
	}
}


void DGHallFiller::PlaceLamp(int Idx)
{
	if (CurrentLamps >= MaxLamps)
	{
		return;
	}
	
	int RandSpan = HallStruct->LampRandSpan + 1;

	int HorRandGap = Gen->GetCurrentStream()->RandRange(0, HallStruct->LampRandSpan);
	int VerRandGap = Gen->GetCurrentStream()->RandRange(0, HallStruct->LampRandSpan);

	for (int i = VerRandGap + HorRandGap * RandSpan; i < RandSpan * RandSpan; i++)
	{
		int H = (i / (RandSpan));
		int R = (i % (RandSpan));

		int CurrIdx = Idx + R + (H * Room->Height);

		TArray<int> OccCells;

		if (IsEnoughPlaceForObject(CurrIdx, HallStruct->LampSizeX, HallStruct->LampSizeY, OccCells))
		{
			DGCell* C = Room->RoomTiles[CurrIdx];
			C->Role = ECellRoomRole::Furniture;
			AActor* Lamp = Gen->GetWorld()->SpawnActor<AActor>(HallStruct->Lamp, C->Position, FRotator::ZeroRotator);
			Lamp->Tags.Add(ADGGenerator::DUNGEON_TAG);
			
			FVector LampPos = C->Position + GetPositionOffsetForLamp(Lamp);
			Lamp->SetActorLocation(LampPos);
			Lamp->SetActorScale3D(GetScaleForLamp(Lamp->GetActorScale3D()));

			CurrentLamps++;
			break;
		}


	}
}


bool DGHallFiller::PickSpotForLampAroundPillar(int PillarIndex, int& LampIndex)
{
	TArray<int> PlaceOptions;
	
	if (IsSameCol(PillarIndex, PillarIndex - 1))
	{
		for (int i = 0; i < HallStruct->PillarSizeX; i++)
		{
			PlaceOptions.Add((PillarIndex - 1) + i * Room->Height);
		}
	}
	
	if (IsSameCol(PillarIndex, PillarIndex + 1))
	{
		for (int i = 0; i < HallStruct->PillarSizeX; i++)
		{
			PlaceOptions.Add((PillarIndex + 1) + i * Room->Height);
		}
	}
	
	if (IsSameRow(PillarIndex, PillarIndex + Room->Height))
	{
		for (int i = 0; i < HallStruct->PillarSizeY; i++)
		{
			PlaceOptions.Add((PillarIndex + Room->Height) + i);
		}
	}
	
	
	if (IsSameRow(PillarIndex, PillarIndex - Room->Height))
	{
		for (int i = 0; i < HallStruct->PillarSizeY; i++)
		{
			PlaceOptions.Add((PillarIndex - Room->Height) + i);
		}
	}

	if (PlaceOptions.Num() < 1)
	{
		return false;
	}

	LampIndex = PlaceOptions[Gen->GetCurrentStream()->RandRange(0, PlaceOptions.Num() - 1)];

	return true;
}



void DGHallFiller::PlaceThrone()
{
	int TestIdx = Gen->GetCurrentStream()->RandRange(0, Room->RoomTiles.Num() - 1);

	bool Spawned = false;
	TArray<int> OccupiedCells;

	for (int i = TestIdx; i < Room->RoomTiles.Num(); i++)
	{

		bool bSpawn = false;


		if (!bIsMainDirHorizontal && IsEnoughPlaceForObject(i, HallStruct->ThroneSizeX, HallStruct->ThroneSizeY, OccupiedCells))
		{
			bSpawn = true;
		}
		else if (IsEnoughPlaceForObject(i, HallStruct->ThroneSizeY, HallStruct->ThroneSizeX, OccupiedCells))
		{
			bSpawn = true;
		}


		if (bSpawn)
		{
			DGCell* C = Room->RoomTiles[i];

			FRotator Rotation = GetRotationForThrone(i);
			FVector Position = C->Position + GetPositionForThrone(Rotation);

			C->Role = ECellRoomRole::Furniture;
			AActor* Throne = Gen->GetWorld()->SpawnActor<AActor>(HallStruct->Throne, Position, Rotation);
			Throne->SetActorScale3D(GetScaleForThrone(Throne->GetActorScale3D()));
			Throne->Tags.Add(ADGGenerator::DUNGEON_TAG);
			break;
		}


		OccupiedCells.Empty();


	}

	for (int I : OccupiedCells)
	{
		Room->RoomTiles[I]->Role = ECellRoomRole::Furniture;
	}

}


FRotator DGHallFiller::GetRotationForThrone(int SpawnCell)
{
	FRotator ResultRotator;
	float MiddleCellRow = 0;
	float MiddleCellCol = 0;
	
	if (bIsMainDirHorizontal)
	{
		ResultRotator = FRotator(0, -90.f, 0);
		MiddleCellCol = GetCellCol(SpawnCell + ((HallStruct->ThroneSizeX - 1) / 2) * Room->Height);

		if (RandomlyRoundFloatNumberIfNotZero(MiddleCellCol) > ((Room->Width - 1) / 2))
		{
			ResultRotator += FRotator(0, 180.f, 0);
		}
	}
	else
	{
		ResultRotator = FRotator::ZeroRotator;
		MiddleCellRow = GetCellRow(SpawnCell + ((HallStruct->ThroneSizeY - 1) / 2));

		if (RandomlyRoundFloatNumberIfNotZero(MiddleCellRow) > ((Room->Height - 1) / 2))
		{
			ResultRotator += FRotator(0, 180.f, 0);
		}
	}

	return ResultRotator;
}



FVector DGHallFiller::GetPositionForThrone(FRotator RotatedThrone)
{
	FVector ResultVector(0,0,0);
	
	if (bIsMainDirHorizontal)
	{
		if (FMath::IsNearlyEqual(RotatedThrone.Yaw, 90))
		{
			ResultVector = FVector(HallStruct->ThroneSizeY * FloorSize, 0, 0);
		}
		else
		{
			ResultVector = FVector(0, HallStruct->ThroneSizeX * FloorSize, 0);
		}
	}
	else
	{
		if (FMath::IsNearlyEqual(RotatedThrone.Yaw, 180))
		{
			ResultVector = FVector(HallStruct->ThroneSizeX * FloorSize, HallStruct->ThroneSizeY * FloorSize, 0);
		}
	}

	return ResultVector;
}


FVector DGHallFiller::GetPositionOffsetForPillar(AActor* Pillar)
{
	if (PillarOffset.Equals(FVector(0)))
	{
		FVector Origin;
		FVector BoxExtent;
		Pillar->GetActorBounds(false, Origin, BoxExtent, true);

		PillarOffset = FVector((Room->FloorSize / 2) - BoxExtent.X, (Room->FloorSize / 2) - BoxExtent.Y, 0);
	}	

	return PillarOffset;
}


FVector DGHallFiller::GetScaleForPillar(FVector PillarScale)
{
	float W = Gen->GetCurrentStream()->FRandRange(HallStruct->MinPillarWidthScale, HallStruct->MaxPillarWidthScale);
	float Z = Gen->GetCurrentStream()->FRandRange(HallStruct->MinPillarHeightScale, HallStruct->MaxPillarHeightScale);
	
	return FVector(PillarScale.X * W, PillarScale.Y * W, PillarScale.Z * Z);
}



FVector DGHallFiller::GetScaleForThrone(FVector ThroneScale)
{
	float W = Gen->GetCurrentStream()->FRandRange(HallStruct->MinThroneScale, HallStruct->MaxThroneScale);
	return ThroneScale * W;
}
