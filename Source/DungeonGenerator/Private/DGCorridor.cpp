

#include "DGCorridor.h"
#include "DGGenerator.h"
#include "DGRoom.h"
#include "DGCell.h"
#include "DrawDebugHelpers.h"
#include "Components/InstancedStaticMeshComponent.h"
#include "Engine/StaticMesh.h" 

// Sets default values
ADGCorridor::ADGCorridor()
{
 	// Set this actor to call Tick() every frame. You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;

	Tags.Add(ADGGenerator::DUNGEON_TAG);
	MeshComp = CreateDefaultSubobject<UInstancedStaticMeshComponent>(TEXT("MeshComp"));

}

// Called when the game starts or when spawned
void ADGCorridor::BeginPlay()
{
	Super::BeginPlay();
	
}

/**
* Builds a corridor Static Mesh Instances from one index to other in X or Y direction
* @param GridIndexStart Index to build from
* @param GridIndexEnd Index to build to
*/
void ADGCorridor::Build(int GridIndexStart, int GridIndexEnd)
{

	int Increment = 1;
	int IdxStartCol = Gen->IdxToCol(GridIndexStart);
	int IdxEndCol = Gen->IdxToCol(GridIndexEnd);
	int IdxStartRow = Gen->IdxToRow(GridIndexStart);
	int IdxEndRow = Gen->IdxToRow(GridIndexEnd);
	FloorSize = Gen->GetFloorSize();
	
	if (IdxStartCol == IdxEndCol)
	{
		Increment = Gen->GetTileGridWidth();

		if (IdxStartRow > IdxEndRow)
		{

			Swap(GridIndexStart, GridIndexEnd);
		}

	}
	else if (GridIndexStart > GridIndexEnd)
	{
		Swap(GridIndexStart, GridIndexEnd);
	}

	if (!GetActorLocation().Equals((*Gen->GetTileGrid())[GridIndexStart].Position)
		&& MeshComp->GetInstanceCount() < 1)
	{
		SetActorLocation((*Gen->GetTileGrid())[GridIndexStart].Position);
	}


	FVector Pos = (*Gen->GetTileGrid())[GridIndexStart].Position - GetActorLocation();

	int LastIndex = -1;

	for (int i = GridIndexStart; i <= GridIndexEnd; i += Increment)
	{
		if (i == GridIndexStart)
		{
			(*Gen->GetTileGrid())[i].bIsCorridorFirst = true;
		}


		switch ((*Gen->GetTileGrid())[i].Type)
		{
		case ECellType::Empty:
		{
			(*Gen->GetTileGrid())[i].Type = ECellType::Corridor;
			(*Gen->GetTileGrid())[i].OccupiedActor = this;
			
			MeshComp->AddInstance(FTransform(FRotator::ZeroRotator, Pos));
			OccupiedCells.Add(i);
		}
		break;

		case ECellType::CRoom:
		{
			// mark room as Normal if it's not Main and continue
			ADGRoom* Room = Cast<ADGRoom>((*Gen->GetTileGrid())[i].OccupiedActor);

			if (Room->RoomType == ERoomType::Unused)
			{
				Room->RoomType = ERoomType::Normal;
			}

			IntersectingRooms.Add(Room);
		}
		break;
		}

		if (Increment > 1) {
			Pos.Y += FloorSize;
		}
		else
		{
			Pos.X += FloorSize;
		}

		LastIndex = i;
	}
	
	if (LastIndex > -1)
	{
		(*Gen->GetTileGrid())[LastIndex].bIsCorridorLast = true;
	}
}



/**
* Builds a L shaped corridor from RoomA to RoomB with randomly selected start
* @param RoomA Index to build from
* @param RoomB Index to build to
*/
void ADGCorridor::BuildLShape(ADGRoom* RoomA, ADGRoom* RoomB)
{
	bool BuildRight = true, BuildUp = true;
	int GridWidth = Gen->GetTileGridWidth();
	TArray<DGCell>* TileGrid = Gen->GetTileGrid();


	if (Gen->IdxToRow(RoomA->GetGridStartIndex()) < Gen->IdxToRow(RoomB->GetGridStartIndex()))
	{
		BuildUp = false;
	}
 
	if (Gen->IdxToCol(RoomA->GetGridStartIndex()) > Gen->IdxToCol(RoomB->GetGridStartIndex()))
	{
		BuildRight = false;
	}


	bool LRFirst = (bool)Gen->GetCurrentStream()->RandRange(0, 1);
	int StartCell, MiddleCell, EndCell;


	if (BuildUp)
	{
		if (BuildRight)
		{
			if (LRFirst)
			{
				StartCell = RoomA->GetGridStartIndex() + RoomA->Width + GridWidth * (RoomA->Height / 2);
				EndCell = RoomB->GetGridStartIndex() + RoomB->Height * GridWidth + (RoomB->Width / 2);
				MiddleCell = StartCell + (Gen->IdxToCol(EndCell) - Gen->IdxToCol(StartCell));
			}
			else
			{
				StartCell = RoomA->GetGridStartIndex() + RoomA->Width / 2;
				EndCell = RoomB->GetGridStartIndex() + (RoomB->Height / 2) * GridWidth;
				MiddleCell = EndCell - (Gen->IdxToCol(EndCell) - Gen->IdxToCol(StartCell));
			}
		}
		else
		{
			if (LRFirst)
			{
				StartCell = RoomA->GetGridStartIndex() + (RoomA->Height / 2) * GridWidth;
				EndCell = RoomB->GetGridStartIndex() + (RoomB->Height) * GridWidth + (RoomB->Width / 2);
				MiddleCell = StartCell - (Gen->IdxToCol(StartCell) - Gen->IdxToCol(EndCell));
			}
			else
			{
				StartCell = RoomA->GetGridStartIndex() + RoomA->Width / 2;
				EndCell = RoomB->GetGridStartIndex() + RoomB->Width + GridWidth * (RoomB->Height / 2);
				MiddleCell = EndCell + (Gen->IdxToCol(StartCell) - Gen->IdxToCol(EndCell));
			}
		}
	}
	else
	{
		if (BuildRight)
		{
			if (LRFirst)
			{
				StartCell = RoomA->GetGridStartIndex() + RoomA->Width + GridWidth * (RoomA->Height / 2);
				EndCell = RoomB->GetGridStartIndex() + RoomB->Width / 2;
				MiddleCell = StartCell + (Gen->IdxToCol(EndCell) - Gen->IdxToCol(StartCell));
			}
			else
			{
				StartCell = RoomA->GetGridStartIndex() + (RoomA->Height) * GridWidth + (RoomA->Width / 2);
				EndCell = RoomB->GetGridStartIndex() + (RoomB->Height / 2) * GridWidth;
				MiddleCell = EndCell - (Gen->IdxToCol(EndCell) - Gen->IdxToCol(StartCell));
			}
		}
		else
		{
			if (LRFirst)
			{
				StartCell = RoomA->GetGridStartIndex() + (RoomA->Height / 2) * GridWidth;
				EndCell = RoomB->GetGridStartIndex() + RoomB->Width / 2;
				MiddleCell = StartCell - (Gen->IdxToCol(StartCell) - Gen->IdxToCol(EndCell));
			}
			else
			{
				StartCell = RoomA->GetGridStartIndex() + (RoomA->Height) * GridWidth + (RoomA->Width / 2);
				EndCell = RoomB->GetGridStartIndex() + RoomB->Width + GridWidth * (RoomB->Height / 2);
				MiddleCell = EndCell + (Gen->IdxToCol(StartCell) - Gen->IdxToCol(EndCell));
			}
		}
	}

	FColor s = FColor::Green;
	FColor m = FColor::Red;
	FColor e = FColor::Blue;

	if (Gen->bDebug)
	{
		DrawDebugPoint(GetWorld(), (*TileGrid)[StartCell].Position, 20.f, s, true);
		DrawDebugPoint(GetWorld(), (*TileGrid)[MiddleCell].Position, 20.f, m, true);
		DrawDebugPoint(GetWorld(), (*TileGrid)[EndCell].Position, 20.f, e, true);
		DrawDebugLine(GetWorld(), (*TileGrid)[StartCell].Position, (*TileGrid)[MiddleCell].Position, FColor::White, true, -1.f, 0, 20.f);
		DrawDebugLine(GetWorld(), (*TileGrid)[MiddleCell].Position, (*TileGrid)[EndCell].Position, FColor::White, true, -1.f, 0, 20.f);
	}

	FColor Col = FColor::MakeRandomColor();

	Build(StartCell, MiddleCell);
	Build(MiddleCell, EndCell);
}

/**
* Initializes this corridor object with StaticMeshes of wall and floor
* @param Generator instance of Generator from which should this function take the data to initialize
*/
void ADGCorridor::Init(ADGGenerator* Generator)
{
	this->Gen = Generator;

	MeshComp->RegisterComponent();
	AddInstanceComponent(MeshComp);

	FloorMesh = Gen->CorridorFloorMesh;
	WallMesh = Gen->CorridorWallMesh;

	FloorSize = Gen->GetFloorSize();

	SetRootComponent(MeshComp);
	MeshComp->SetStaticMesh(FloorMesh);
}


/**
* Spawns and initializes InstancedStaticMeshComponent for walls
*/
void ADGCorridor::SpawnWallsComponent()
{
	WallInstMeshComponent = NewObject<UInstancedStaticMeshComponent>(this, FName("WallInstancedStaticMesh"));

	WallInstMeshComponent->RegisterComponent();
	WallInstMeshComponent->SetStaticMesh(WallMesh);
	AddInstanceComponent(WallInstMeshComponent);
	WallInstMeshComponent->AttachToComponent(RootComponent, FAttachmentTransformRules::KeepRelativeTransform);

	FVector Loc = FVector(0, 0, 0);
	Loc.Z += FloorMesh->GetBoundingBox().GetExtent().Z;

	WallInstMeshComponent->SetRelativeLocation(Loc);
}

/**
* Builds InstancedStaticMeshComponent instances around the corridor
*/
void ADGCorridor::BuildWalls()
{
	if (!WallMesh)
	{
		return;
	}


	SpawnWallsComponent();
	int GridWidth = Gen->GetTileGridWidth();


	for (int i = 0; i < OccupiedCells.Num(); i++)
	{

		// checking above the index where we want to build the wall
		int CurrIdx = OccupiedCells[i];
		int CurrCol = Gen->IdxToCol(CurrIdx);

		FTransform WallPosition;
		MeshComp->GetInstanceTransform(i, WallPosition);

		// are we in the grid?


		if (ShouldBuildWallInDirectionToIndex(CurrIdx, CurrIdx - GridWidth))
		{
			BuildWallAt(EWallPosition::Up, WallPosition.GetLocation());
		}

		if (ShouldBuildWallInDirectionToIndex(CurrIdx, CurrIdx + GridWidth))
		{
			BuildWallAt(EWallPosition::Down, WallPosition.GetLocation());
		}

		if ((CurrCol - 1) != Gen->IdxToCol(CurrIdx - 1) || ShouldBuildWallInDirectionToIndex(CurrIdx, CurrIdx - 1))
		{
			BuildWallAt(EWallPosition::Left, WallPosition.GetLocation());
		}

		if ((CurrCol + 1) != Gen->IdxToCol(CurrIdx + 1) || ShouldBuildWallInDirectionToIndex(CurrIdx, CurrIdx + 1))
		{
			BuildWallAt(EWallPosition::Right, WallPosition.GetLocation());
		}
	}



}

/**
* Checks if a wall should be built from direction of one index to other
* @param CurrIndex Index where the wall is being built
* @param DirIndex Index of cell next to CurrIndex to check for building
* @return Returns true if wall should be built, false otherwise
*/
bool ADGCorridor::ShouldBuildWallInDirectionToIndex(int CurrIndex, int DirIndex)
{

	if (DirIndex < 0 || DirIndex >((*Gen->GetTileGrid()).Num() - 1))
	{
		return false;
	}


	DGCell DirCell = (*Gen->GetTileGrid())[DirIndex];
	DGCell CurrCell = (*Gen->GetTileGrid())[CurrIndex];

	if (DirCell.Type == ECellType::Empty)
	{
		return true;
	}
	else if (DirCell.Type == ECellType::CRoom)
	{
		if (!Gen->bBuildWallsAroundRooms)
		{
			return false;
		}

		
		if (CurrCell.bIsCorridorFirst || CurrCell.bIsCorridorLast)
		{
			return false;
		}
		else if (IntersectingRooms.Contains(DirCell.OccupiedActor))
		{
			return false;
		}

		return true;
	}

	return false;
}


/**
* Builds wall at correct location and orientation
* @param WallPos Position of wall relative to cell
* @param DirIndex Index of cell next to CurrIndex to check for building
*/
void ADGCorridor::BuildWallAt(EWallPosition WallPos, FVector Location)
{

	FRotator Rot;

	switch (WallPos)
	{
		case EWallPosition::Up:
		{
			Rot = FRotator(0, 0, 0);
			break;

		}
		case EWallPosition::Down:
		{
			Rot = FRotator(0, 0, 0);
			Location.Y += FloorSize - WallMesh->GetBoundingBox().GetExtent().Y * 2;
			break;

		}
		case EWallPosition::Left:
		{
			Rot = FRotator(0, 90, 0);
			Location.X += WallMesh->GetBoundingBox().GetExtent().Y * 2;

			break;
		}
		case EWallPosition::Right:
		{
			Rot = FRotator(0, 90, 0);
			Location.X += FloorSize;
			break;

		}

	}


	WallInstMeshComponent->AddInstance(FTransform(Rot, Location));
}