// Fill out your copyright notice in the Description page of Project Settings.

#include "DGGenerator.h"
#include "DrawDebugHelpers.h"
#include "MinimumSpanningTree.h"
#include "Kismet/KismetSystemLibrary.h"
#include "Kismet/KismetMathLibrary.h"
#include "Kismet/GameplayStatics.h"
#include "Engine/StaticMeshActor.h"
#include "DGRoom.h"
#include "DGCell.h"
#include "DGCorridor.h"
#include "DGLibraryFiller.h"
#include "DGHallFiller.h"
#include <delaunator.hpp>


const FName ADGGenerator::DUNGEON_TAG = FName("Dungeon_Generator_Sami");

DEFINE_LOG_CATEGORY(DungeonGenerator);

// Sets default values
ADGGenerator::ADGGenerator()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;

}


// Called when the game starts or when spawned
void ADGGenerator::BeginPlay()
{
	Super::BeginPlay();
}



/**
* Runs the whole generation process. Is run from the Generate button
*/
void ADGGenerator::Generate()
{

	FillRoomStructs();

	if (!CheckParameters())
	{
		return;
	}

	Clear();

	
	if (!bCustomSeed)
	{
		Seed = FMath::Rand();
	}
	
	OnBeforeGenerationStarts.Broadcast(Seed);


	Stream = FRandomStream(Seed);


	for (FDGRoomStruct* RoomS : RoomStructs)
	{
		if (RoomS->bIgnoreThisType)
		{
			continue;
		}

		FloorSize = GetSizeOfFloorMesh(RoomStructs[0]->FloorMesh);
	}



	TArray<FVector> SpawnPoints = GetRandomPointsInGrid(FVector::ZeroVector, CircleRadius, RoomsCount);


	SpawnedRooms = SpawnRooms(SpawnPoints);

	MainRooms = SelectMainRooms();

	SeparateRooms();

	FDGGraph DelaunayGraph = CreateDelaunayGraph();

	FDGGraph MST = MinimumSpanningTree::ConvertGraphToMST(DelaunayGraph);

	RenewSomeRoomsLinkage(DelaunayGraph, &MST);

	for (ADGRoom* Room : SpawnedRooms)
	{
		Room->BecomeTiles();
	}
	
	FillTileGrid();

	SpawnedCorridors = SpawnCorridors(MST);

	RemoveUnusedRooms();

	SpawnWalls();

	if (bWantToEquipRooms || bSpawnRoomWalls)
	{
		EquipRooms();
	}


	OnAfterGenerationEnds.Broadcast(Seed);

}


/**
* Generates randomly located points in grid
* @param Origin Origin of grid
* @param Radius specifying X and Y extents of grid 
* @param Count How many points should be generated
* @return TArray of FVectors representing random points to spawn rooms on
*/
TArray<FVector> ADGGenerator::GetRandomPointsInGrid(FVector Origin, int Radius, int Count)
{

	TArray<FVector> Points;


	for (uint16 i = 0; i < Count; i++)
	{

		float X = Origin.X + Stream.RandRange(-Radius, Radius) * FloorSize;
		float Y = Origin.Y + Stream.RandRange(-Radius, Radius) * FloorSize;

		Points.Add(FVector(X, Y, GetActorLocation().Z));
		if (bDebug)
		{
			DrawDebugPoint(GetWorld(), Points.Last(), 50.f, FColor::Green, true);
		}
	}

	return Points;
}


/**
* Spawns and initializes rooms on locations
* @param Locations Locations to spawn rooms on
* @return TArray of pointers to spawned ADGRooms
*/
TArray<ADGRoom*> ADGGenerator::SpawnRooms(TArray<FVector> Locations)
{
	TArray<ADGRoom*> NewRooms;

	for (FVector Location : Locations)
	{
		ADGRoom* CurrentRoom = GetWorld()->SpawnActor<ADGRoom>(ADGRoom::StaticClass(), FTransform(FRotator(0, 0, 0), Location));
		CurrentRoom->Init(this);
		NewRooms.Add(CurrentRoom);
	}

	return NewRooms;
}



/**
* Separates overlapping rooms held in SpawnedRooms TArray
*/
void ADGGenerator::SeparateRooms()
{

	bool bRoomsSeparated = false;

	while (!bRoomsSeparated)
	{
		bRoomsSeparated = true;

		for (ADGRoom* Room : SpawnedRooms)
		{
			TArray<AActor*> OverlappingRooms;
			UKismetSystemLibrary::ComponentOverlapActors(Cast<UPrimitiveComponent>(Room->GetRootComponent()), Room->GetRootComponent()->GetComponentTransform(),
				TArray<TEnumAsByte<EObjectTypeQuery>>(), NULL, TArray<AActor*>(), OverlappingRooms);



			if (OverlappingRooms.Num() < 1)
			{
				continue;
			}

			bRoomsSeparated = false;

			FVector Movement = FVector::ZeroVector;

			for (AActor* OverlappingRoom : OverlappingRooms)
			{
				ADGRoom* Temp;
				if ((Temp = Cast<ADGRoom>(OverlappingRoom)) != nullptr)
				{
					Movement += Temp->GetCenter() - Room->GetCenter();
				}
			}

			FVector FinalMovement = (ClampVector(Movement, FVector(-FloorSize, -FloorSize, 0), FVector(FloorSize, FloorSize, 0)) * -1.f) * SeparationSpeed;

			FinalMovement = FinalMovement.GridSnap(FloorSize);

			Room->SetActorLocation(Room->GetActorLocation() + FinalMovement);

			if (!Room->HandlePositionCheck())
			{
				Cast<UStaticMeshComponent>(Room->GetRootComponent())->SetCollisionResponseToAllChannels(ECollisionResponse::ECR_Ignore);
				Room->RoomType = ERoomType::Unused;
		
			}
		}
			
	}
	
}


float ADGGenerator::GetAverageRoomSize()
{
	float Sum = 0;

	for (AActor* Room : SpawnedRooms)
	{
		Sum += Room->GetActorScale3D().SizeSquared();
	}

	return Sum / SpawnedRooms.Num();
}



/**
* Selects main rooms based on average room size and MainRoomMultiplier parameter
* @return TArray of pointers to ADGRoom which are big enough to become Main rooms
*/
TArray<ADGRoom*> ADGGenerator::SelectMainRooms()
{

	TArray<ADGRoom*> SelectedRooms;

	float AverageSize = GetAverageRoomSize() * MainRoomMultiplier;

	for (ADGRoom* Room : SpawnedRooms)
	{

		if (Room->GetActorScale3D().SizeSquared() > AverageSize)
		{
			SelectedRooms.Add(Room);
			Room->RoomType = ERoomType::Main;
		}
	}

	return SelectedRooms;
}



/**
* Spawns corridors based on the nodes and edges in FDGGraph
* @param MST Graph of connections between rooms
* @return TArray of pointers to spawned ADGCorridors 
*/
TArray<ADGCorridor*> ADGGenerator::SpawnCorridors(FDGGraph MST)
{

	bool bFIrst = true;

	TArray<ADGCorridor*> Corridors;

	for (FDGVertex RoomVertex : MST.Vertices)
	{
		ADGRoom* Room = *MainRooms.FindByPredicate([RoomVertex](ADGRoom* InRoom) {
			return InRoom->GetActorLocation().Equals(RoomVertex.Position);
			});


		TArray<FDGEdge> VertexEdges = MST.Edges.FilterByPredicate([RoomVertex](FDGEdge InEdge) {
			return InEdge.V1 == RoomVertex;
			});


		for (FDGEdge Edge : VertexEdges)
		{
			ADGRoom* ConnectedRoom = *MainRooms.FindByPredicate([Edge, Room](ADGRoom* InRoom) {
				FDGVertex ToComp = Edge.V1;

				if (Edge.V1.Position.Equals(Room->GetActorLocation()))
				{
					ToComp = Edge.V2;
				}

				return ToComp.Position.Equals(InRoom->GetActorLocation());

				});

			FString TestName = Room->GetName();

			int StartIndex, EndIndex;
			ADGCorridor* Corridor;

			if (GetRowIndexesIfRoomsOverlap(Room, ConnectedRoom, StartIndex, EndIndex) ||
				GetColumnIndexesIfRoomsOverlap(Room, ConnectedRoom, StartIndex, EndIndex))
			{
				Corridor = GetWorld()->SpawnActor<ADGCorridor>(ADGCorridor::StaticClass(), TileGrid[StartIndex].Position, FRotator::ZeroRotator);
				Corridor->Init(this);
				Corridor->Build(StartIndex, EndIndex);
			}
			else
			{
				Corridor = GetWorld()->SpawnActor<ADGCorridor>(ADGCorridor::StaticClass(), FVector(0,0,0), FRotator::ZeroRotator);
				Corridor->Init(this);
				Corridor->BuildLShape(Room, ConnectedRoom);
			}

			Corridors.Add(Corridor);

		}

	}

	return Corridors;
}


/**
* Checks if 2 rooms overlap on X axis and sets positions for potentional Corridor to spawn
* @param RoomA First room to check overlap
* @param RoomB Second room to check overlap with
* @param StartIndex is set as a first index of corridor which will be built
* @param EndIndex is set as a end index of corridor which will be built
* @return bool true if rooms overlap
*/
bool ADGGenerator::GetRowIndexesIfRoomsOverlap(ADGRoom* RoomA, ADGRoom* RoomB, int& StartIndex, int& EndIndex)
{
	int AMinY = IdxToRow(RoomA->GetGridStartIndex());
	int AMaxY = AMinY + (RoomA->Height - 1);

	int BMinY = IdxToRow(RoomB->GetGridStartIndex());
	int BMaxY = BMinY + (RoomB->Height - 1);

	int OverlapStart = FMath::Max(AMinY, BMinY);
	int OverlapEnd = FMath::Min(AMaxY, BMaxY);

	int Res = OverlapEnd - OverlapStart;

	if (Res < 0)
	{
		return false;
	}

	// returns index of row where the hallway should begin and end
	Res = OverlapStart + (Res / 2);


	int AStartX = IdxToCol(RoomA->GetGridStartIndex());
	int BStartX = IdxToCol(RoomB->GetGridStartIndex());

	if (AStartX < BStartX)
	{
		StartIndex = AStartX + RoomA->Width + (Res * TileGridWidth);
		EndIndex = BStartX + (Res * TileGridWidth);
	}
	else
	{
		StartIndex = BStartX + RoomB->Width + (Res * TileGridWidth);
		EndIndex = AStartX + (Res * TileGridWidth);
	}

	return true;
}


/**
* Checks if 2 rooms overlap on Y axis and sets positions for potentional Corridor to spawn
* @param RoomA First room to check overlap
* @param RoomB Second room to check overlap with
* @param StartIndex is set as a first index of corridor which will be built
* @param EndIndex is set as a end index of corridor which will be built
* @return bool true if rooms overlap
*/
bool ADGGenerator::GetColumnIndexesIfRoomsOverlap(ADGRoom* RoomA, ADGRoom* RoomB, int& StartIndex, int& EndIndex)
{
	int AMinX = IdxToCol(RoomA->GetGridStartIndex());
	int AMaxX = AMinX + (RoomA->Width - 1);

	int BMinX = IdxToCol(RoomB->GetGridStartIndex());
	int BMaxX = BMinX + (RoomB->Width - 1);

	int OverlapStart = FMath::Max(AMinX, BMinX);
	int OverlapEnd = FMath::Min(AMaxX, BMaxX);

	int Res = OverlapEnd - OverlapStart;

	if (Res < 0)
	{
		return false;
	}

	// returns index of column where the hallway should begin and end
	Res = OverlapStart + (Res / 2);

	int AStartY = IdxToRow(RoomA->GetGridStartIndex());
	int BStartY = IdxToRow(RoomB->GetGridStartIndex());

	if (AStartY < BStartY)
	{
		StartIndex = (AStartY + RoomA->Height) * TileGridWidth + Res;
		EndIndex = (BStartY * TileGridWidth) + Res;
	}
	else
	{
		StartIndex = (BStartY + RoomB->Height) * TileGridWidth + Res;
		EndIndex = (AStartY * TileGridWidth) + Res;
	}

	return true;
}

/**
* Creates FDGGraph that represents Delaunay Triangulation between SpawnedRooms
* @return FDGGraph
*/
FDGGraph ADGGenerator::CreateDelaunayGraph()
{
	std::vector<double> Coords;

	for (ADGRoom* Room : MainRooms)
	{
		Coords.push_back(Room->GetActorLocation().X);
		Coords.push_back(Room->GetActorLocation().Y);
	}


	FDGGraph Graph;
	float CurrZ = GetActorLocation().Z;

	// if we have no triangle, we create graph without delaunay
	if (Coords.size() < 6) {

		FDGVertex LastVertex;

		for (size_t i = 0; i < Coords.size(); i += 2) {
			FDGVertex v0(FVector(Coords.at(i), Coords.at(i + 1), CurrZ));

			if (i > 0) {
				Graph.Edges.Add(FDGEdge(LastVertex, v0, (LastVertex.Position - v0.Position).Size()));
			}

			LastVertex = v0;

			Graph.Vertices.AddUnique(v0);
		}

		return Graph;
	}

	delaunator::Delaunator d(Coords);


	for (size_t i = 0; i < d.triangles.size(); i += 3)
	{

		double tx0 = d.coords[2 * d.triangles[i]];
		double ty0 = d.coords[2 * d.triangles[i] + 1];     //ty0
		double tx1 = d.coords[2 * d.triangles[i + 1]];     //tx1
		double ty1 = d.coords[2 * d.triangles[i + 1] + 1]; //ty1
		double tx2 = d.coords[2 * d.triangles[i + 2]];     //tx2
		double ty2 = d.coords[2 * d.triangles[i + 2] + 1]; //ty2


		FVector t0 = FVector(tx0, ty0, CurrZ);
		FVector t1 = FVector(tx1, ty1, CurrZ);
		FVector t2 = FVector(tx2, ty2, CurrZ);

		FDGVertex v0(t0);
		FDGVertex v1(t1);
		FDGVertex v2(t2);

		Graph.Vertices.AddUnique(v0);
		Graph.Vertices.AddUnique(v1);
		Graph.Vertices.AddUnique(v2);

		Graph.Edges.Add(FDGEdge(v0, v1, (t1 - t0).Size()));
		Graph.Edges.Add(FDGEdge(v1, v2, (t2 - t1).Size()));
		Graph.Edges.Add(FDGEdge(v2, v0, (t0 - t2).Size()));

	}

	if (bDebug)
	{

		for (int i = 0; i < Graph.Edges.Num(); ++i) {
			DrawDebugLine(GetWorld(), Graph.Edges[i].V1.Position, Graph.Edges[i].V2.Position, FColor::Blue, true, -1.f, 0U, 70.f);
		}
	}

	return Graph;
}


/**
* Restores some proportion of edges from the OriginalDelaunayGraph
* @param FDGGraph graph from which the edges should be restored
* @param FDGGraph graph to which the edges should be added
*/
void ADGGenerator::RenewSomeRoomsLinkage(FDGGraph OriginalDelaunayGraph, FDGGraph* MinimalSpanningTree)
{
	// Renew some of the edges so the dungeon becomes more cyclic

	int RenewNum = FMath::Min(FMath::CeilToInt(OriginalDelaunayGraph.Edges.Num() * (RoomsLinkage / 100)), OriginalDelaunayGraph.Edges.Num());

	OriginalDelaunayGraph.Edges.Sort([this](FDGEdge e1, FDGEdge e2) {
			return (bool)Stream.RandRange(0, 1);
		});


	for (int i = 0; i < RenewNum; i++)
	{
		FDGEdge ToRenew = OriginalDelaunayGraph.Edges[i];

		MinimalSpanningTree->Edges.AddUnique(ToRenew);

	}

	if (bDebug)
	{
		for (FDGEdge E : MinimalSpanningTree->Edges)
		{
			DrawDebugLine(GetWorld(), E.V1.Position, E.V2.Position, FColor::Purple, true, -1.f, 0U, 50.f);
		}
	}

}

/**
* Creates TArray representing grid over spawned ADGRooms (from minimum [X,Y] to maximum [X,Y])
*/
void ADGGenerator::FillTileGrid()
{
	ADGRoom* FirstRoom = SpawnedRooms[0];
	FVector2D Minimal(FirstRoom->GetActorLocation());
	FVector2D Maximal = FVector2D(FirstRoom->GetActorLocation()) + FVector2D(FirstRoom->Height * FloorSize, FirstRoom->Width * FloorSize);

	for (ADGRoom* Room : SpawnedRooms)
	{
		FVector Loc = Room->GetActorLocation();
		Minimal.X = FMath::Min(Loc.X, Minimal.X);
		Minimal.Y = FMath::Min(Loc.Y, Minimal.Y);
		Maximal.X = FMath::Max(Loc.X + Room->Width * FloorSize, Maximal.X);
		Maximal.Y = FMath::Max(Loc.Y + Room->Height * FloorSize, Maximal.Y);
	}

	TileGridWidth = FMath::RoundToInt((Maximal.X - Minimal.X) / FloorSize);
	TileGridHeight = FMath::RoundToInt((Maximal.Y - Minimal.Y) / FloorSize);

	FVector PosBase = FVector(Minimal, GetActorLocation().Z);

	for (size_t i = 0; i < TileGridWidth * TileGridHeight; i++)
	{
		TileGrid.Add(DGCell(ECellType::Empty, PosBase + FVector(IdxToCol(i) * FloorSize, IdxToRow(i) * FloorSize, 0.f), i, nullptr));
	}

	AddSpawnedRoomsToTileGrid(Minimal);
	if (ADGGenerator::bDebug)
	{
		PrintDebugCells();
	}
}


/**
* Inserts already spawned ADGRooms to tile grid based on their position
* @param MinimalGridPosition smallest position of the grid from which the concrete cells of rooms should be measured
*/
void ADGGenerator::AddSpawnedRoomsToTileGrid(FVector2D MinimalGridPosition)
{
	for (ADGRoom* Room : SpawnedRooms)
	{
		FVector Pos = Room->GetActorLocation();
		int RoomStartIndexX = FMath::RoundToInt(FMath::Abs(FMath::Abs(MinimalGridPosition.X) + Pos.X) / FloorSize);
		int RoomStartIndexY = FMath::RoundToInt(FMath::Abs(FMath::Abs(MinimalGridPosition.Y) + Pos.Y) / FloorSize);

		Room->SetGridStartIndex(TileGridWidth * RoomStartIndexY + RoomStartIndexX);

		for (int w = 0; w < Room->Width; w++)
		{
			for (int h = 0; h < Room->Height; h++)
			{
				DGCell *C = &TileGrid[Room->GetGridStartIndex() + h * (TileGridWidth)+w];
				C->OccupiedActor = Room;
				C->Type = ECellType::CRoom;
				C->RoomIndex = h + w * Room->Height;

				Room->RoomTiles.Add(C);
			}
		}
	}
}

/**
* Removes rooms that are not connected to anything
*/
void ADGGenerator::RemoveUnusedRooms()
{

	TArray<ADGRoom*> OnlyUsedRooms;
	
	for (ADGRoom* Room : SpawnedRooms)
	{
		if (Room->RoomType == ERoomType::Unused)
		{
			Room->DestroyAndCleanUp();
		} 
		else
		{
			Room->ReclaimTilesIfNeeded();
			OnlyUsedRooms.Add(Room);
		}
	}
	
	SpawnedRooms.Empty();
	SpawnedRooms = OnlyUsedRooms;
}


/**
* Spawns walls on corridors
*/
void ADGGenerator::SpawnWalls()
{

	if (bSpawnCorridorWalls)
	{
		for (ADGCorridor* Corridor : SpawnedCorridors)
		{
			Corridor->BuildWalls();
		}
	}

}


void ADGGenerator::EquipRooms()
{

	for (ADGRoom* Room : SpawnedRooms)
	{
		Room->Equip();
	}
}


void ADGGenerator::Clear()
{
	
	TArray<AActor*> ActorsToDestroy;
	UGameplayStatics::GetAllActorsWithTag(GetWorld(), ADGGenerator::DUNGEON_TAG, ActorsToDestroy);

	for (AActor* Atd : ActorsToDestroy)
	{
		Atd->Destroy();
	}

	TileGrid.Empty();
	SpawnedRooms.Empty();
	MainRooms.Empty();
	DestroyedRooms.Empty();
	SpawnedCorridors.Empty();
	RoomLocations.Empty();
}


bool ADGGenerator::CheckParameters()
{
	
	float InitialFloorSize = -1;

	int TypesIgnored = 0;

	for (FDGRoomStruct* T : RoomStructs)
	{
		if (T->bIgnoreThisType)
		{
			TypesIgnored++;
			continue;
		}

		if (T->FloorMesh == nullptr)
		{
			UE_LOG(DungeonGenerator, Error, TEXT("Missing floor mesh in one of the room types. Please check that all floor meshes are set."));
			return false;
		}

		if (InitialFloorSize < 0)
		{
			InitialFloorSize = GetSizeOfFloorMesh(T->FloorMesh);
		}

		if (!FMath::IsNearlyEqual(InitialFloorSize, GetSizeOfFloorMesh(T->FloorMesh)))
		{
			UE_LOG(DungeonGenerator, Error, TEXT("Floor meshes vary in size. Please ensure all floor meshes are the same size."));
			return false;
		}

	}

	if (TypesIgnored == RoomStructs.Num())
	{
		UE_LOG(DungeonGenerator, Error, TEXT("At least one of the rooms must not be ignored to generate the layout properly."));
		return false;
	}


	if (CorridorFloorMesh == nullptr)
	{
		UE_LOG(DungeonGenerator, Error, TEXT("Missing floor mesh for corridors. Please check if corridor floor mesh is set."));
		return false;
	}


	if (!FMath::IsNearlyEqual(InitialFloorSize, GetSizeOfFloorMesh(CorridorFloorMesh)))
	{
		UE_LOG(DungeonGenerator, Error, TEXT("Size of corridor floor mesh and room floor meshes differ. Please ensure corridor floor mesh and room floor meshes are the same size."));
		return false;
	}

	return true;
}


DGCell ADGGenerator::GetCellAtPosition(int X, int Y)
{
	return TileGrid[Y * TileGridWidth + X];
}


void ADGGenerator::SetCellAtPosition(int X, int Y, DGCell Cell)
{
	int idx = Y * TileGridWidth + X;
	TileGrid[Y * TileGridWidth + X] = Cell;
}


int ADGGenerator::GetTileGridWidth()
{	
	return TileGridWidth;
}


float ADGGenerator::GetFloorSize()
{	
	return FloorSize;
}


TArray<DGCell>* ADGGenerator::GetTileGrid()
{
	return &TileGrid;
}


FRandomStream* ADGGenerator::GetCurrentStream()
{
	return &Stream;
}


float ADGGenerator::GetSizeOfFloorMesh(UStaticMesh* Mesh)
{
	FBox BoundingBox = Mesh->GetBoundingBox();
	return BoundingBox.GetExtent().X * 2;
}


int ADGGenerator::IdxToRow(int Index)
{
	return Index / TileGridWidth;
}


int ADGGenerator::IdxToCol(int Index)
{
	return Index % TileGridWidth;
}


void ADGGenerator::PrintDebugCells()
{
	for (int i = 0; i < TileGrid.Num(); i++)
	{

		if (TileGrid[i].OccupiedActor && TileGrid[i].OccupiedActor->GetName().Compare("DGRoom_59") == 0)
		{
			if (TileGrid[i].Type == ECellType::CRoom)
				DrawDebugPoint(GetWorld(), TileGrid[i].Position, 20.f, FColor::Magenta, true);
			else
				DrawDebugPoint(GetWorld(), TileGrid[i].Position + FVector(0, 0, 50.f), 20.f, FColor::Yellow, true);
		}
	}
}


bool ADGGenerator::IsActorForObjectSet(TSubclassOf<AActor> CheckedObject, FString CheckedObjectName, bool bWithMessage)
{
	if (!CheckedObject)
	{
		if (bWithMessage) 
		{
			UE_LOG(DungeonGenerator, Warning, TEXT("Actor for \'%s\' was not set so it will not be spawned."), *CheckedObjectName)
		}
		
		return false;
	}

	return true;
}


void ADGGenerator::FillRoomStructs()
{
	RoomStructs.Empty();
	RoomStructs.Add(&HallStruct);
	RoomStructs.Add(&LibraryStruct);
}