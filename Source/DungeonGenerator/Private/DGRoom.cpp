// Fill out your copyright notice in the Description page of Project Settings.


#include "DGRoom.h"
#include "DrawDebugHelpers.h"
#include "GameFramework/Actor.h"
#include "Components/StaticMeshComponent.h"
#include "Engine/StaticMeshActor.h"
#include "DGGenerator.h"
#include "DGCorridor.h"
#include "DGRoomTemplate.h"
#include "DGCell.h"
#include "DGLibraryFiller.h"
#include "DGRoomFiller.h"
#include "DGHallFiller.h"
#include "DGLibraryStruct.h"
#include "DGRoomStruct.h"
#include "DGHallStruct.h"
#include "Materials/MaterialInstanceDynamic.h"

// Sets default values
ADGRoom::ADGRoom()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;
	RoomType = ERoomType::Unused;
	MeshComp = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("MeshComp"));
	RootComponent = MeshComp;
	Tags.Add(ADGGenerator::DUNGEON_TAG);

	LastPosition = GetActorLocation();
	PreLastPosition = LastPosition;

}

// Called when the game starts or when spawned
void ADGRoom::BeginPlay()
{
	Super::BeginPlay();


}


FVector ADGRoom::GetCenter()
{
	float StartX = GetActorLocation().X;
	float StartY = GetActorLocation().Y;


	FVector Center(StartX + ((Width * FloorSize) / 2), StartY + ((Height * FloorSize) / 2), GetActorLocation().Z);

	return Center;
}


void ADGRoom::BecomeTiles()
{
	SetActorScale3D(FVector::OneVector);

	UMaterialInstanceDynamic* Mat = Cast<UMaterialInstanceDynamic>((Cast<UStaticMeshComponent>(RootComponent))->GetMaterial(0));
	Cast<UStaticMeshComponent>(RootComponent)->SetStaticMesh(nullptr);

	FloorInstMeshComponent = NewObject<UInstancedStaticMeshComponent>(this);
	FloorInstMeshComponent->SetWorldLocation(RootComponent->GetComponentLocation());

	FloorInstMeshComponent->RegisterComponent();
	FloorInstMeshComponent->SetStaticMesh(RoomStruct->FloorMesh);
	AddInstanceComponent(FloorInstMeshComponent);
		
	
	for (int h = 0; h < Height; h++)
	{
		for (int w = 0; w < Width; w++)
		{
			FloorInstMeshComponent->AddInstance(FTransform(FRotator::ZeroRotator, FVector((w * FloorSize), (h * FloorSize), 0)));
		}
	}
	

	RootComponent->DestroyComponent();
	SetRootComponent(FloorInstMeshComponent);
}


void ADGRoom::Init(ADGGenerator* Generator)
{

	Gen = Generator;

	FloorSize = Gen->GetFloorSize();

	SelectRoomFiller();

	Width = Gen->GetCurrentStream()->RandRange(Gen->MinRoomWidth, Gen->MaxRoomWidth);
	Height = Gen->GetCurrentStream()->RandRange(Gen->MinRoomHeight, Gen->MaxRoomHeight);

	MeshComp->SetStaticMesh(RoomStruct->FloorMesh);

	SetActorScale3D(FVector(Width, Height, 1.f));

}


void ADGRoom::SetGridStartIndex(int index)
{
	GridStartIndex = index;
}


int ADGRoom::GetGridStartIndex()
{
	return GridStartIndex;
}


void ADGRoom::DestroyAndCleanUp()
{
	for (int w = 0; w < Width; w++)
	{
		for (int h = 0; h < Height; h++)
		{
			DGCell* C = &((*Gen->GetTileGrid())[GetGridStartIndex() + w + (h * Gen->GetTileGridWidth())]);
			C->Type = ECellType::Empty;
			C->Role = ECellRoomRole::Free;
			C->OccupiedActor = nullptr;
		}
	}
	Destroy();
}


bool ADGRoom::HandlePositionCheck()
{
	if (GetActorLocation().Equals(PreLastPosition)) {
		
		if ((++TimesAtTheSamePosition) > 100)
		{

			return false;
		}
	}
	else 
	{
		TimesAtTheSamePosition = 0;
	}

	PreLastPosition = LastPosition;
	LastPosition = GetActorLocation();

	return true;
}



void ADGRoom::SelectRoomFiller()
{
	int N = Gen->GetCurrentStream()->RandRange(0, 1);

	if (N == 0)
	{
		if (!Gen->LibraryStruct.bIgnoreThisType)
		{
			RoomFiller = new DGLibraryFiller();
			RoomStruct = &Gen->LibraryStruct;
		}
		else
		{
			RoomFiller = new DGHallFiller();
			RoomStruct = &Gen->HallStruct;
		}
	}

	if (N == 1)
	{
		if (!Gen->HallStruct.bIgnoreThisType)
		{

			RoomFiller = new DGHallFiller();
			RoomStruct = &Gen->HallStruct;
		}
		else
		{
			RoomFiller = new DGLibraryFiller();
			RoomStruct = &Gen->LibraryStruct;
		}
	}
}


void ADGRoom::Equip()
{
	RoomFiller->Run(this);
}


void ADGRoom::SpawnWallsComponent(UStaticMesh* WallMesh)
{
	WallInstMeshComponent = NewObject<UInstancedStaticMeshComponent>(this, FName("WallInstancedStaticMesh"));

	WallInstMeshComponent->RegisterComponent();
	WallInstMeshComponent->SetStaticMesh(WallMesh);
	AddInstanceComponent(WallInstMeshComponent);
	WallInstMeshComponent->AttachToComponent(GetRootComponent(), FAttachmentTransformRules::KeepRelativeTransform);

	FVector Loc = FVector(0, 0, 0);
	Loc.Z += RoomStruct->FloorMesh->GetBoundingBox().GetExtent().Z;

	WallInstMeshComponent->SetRelativeLocation(Loc);
}


void ADGRoom::ReclaimTilesIfNeeded()
{
	if (TimesAtTheSamePosition < 1)
	{
		return;
	}

	for (int i = 0; i < RoomTiles.Num(); i++)
	{
		DGCell* C = RoomTiles[i];
		C->Type = ECellType::CRoom;
		C->Role = ECellRoomRole::Free;
		C->OccupiedActor = this;
		C->RoomIndex = i;
	}
}
