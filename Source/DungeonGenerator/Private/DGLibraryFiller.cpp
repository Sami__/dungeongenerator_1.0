// Fill out your copyright notice in the Description page of Project Settings.


#include "DGLibraryFiller.h"
#include "DGLibraryStruct.h"
#include "DGRoom.h"
#include "DGCell.h"
#include "DGGenerator.h"
#include "DGCorridor.h"

void DGLibraryFiller::Run(ADGRoom* RoomToFill)
{
	DGRoomFiller::Run(RoomToFill);

	if (!Gen->bWantToEquipRooms)
	{
		return;
	}

	LibraryStruct = &Gen->LibraryStruct;


	BuildEntranceWalkways();
	BuildMainDirWalkways();	
	BuildSideDirWalkways();

	if (Room->Gen->IsActorForObjectSet(LibraryStruct->Library, FString("Library")))
	{
		PlaceLibraries();
	}

	if (Room->Gen->IsActorForObjectSet(LibraryStruct->Lamp, FString("Lamp")))
	{
		PlaceLamps();
	} 
}


void DGLibraryFiller::BuildEntranceWalkways()
{
	TArray<DGCell*> EntranceCells = Room->RoomTiles.FilterByPredicate([] (DGCell* Cell) {
		return Cell->Role == ECellRoomRole::Entrance;
	}); 
	
	for (DGCell* Cell : EntranceCells)
	{
		int Increment = 1;

		int StartIndex = Cell->Index;

		// Above
		if (IsEntranceFromDirection(Cell->Index - Gen->GetTileGridWidth()))
		{
			Increment = Gen->GetTileGridWidth();
		}
		// Left
		else if (IsEntranceFromDirection(Cell->Index - 1))
		{
			Increment = 1;
		}
		// Right
		else if (IsEntranceFromDirection(Cell->Index + 1))
		{
			Increment = -1;
		}
		// Below
		else
		{
			Increment = -Gen->GetTileGridWidth();
		}

		if (bIsMainDirHorizontal)
		{
			if (FMath::Abs(Increment) == 1)
			{
				SideDirWalkways++;
			}

		}
		else
		{
			if (FMath::Abs(Increment) > 1)
			{
				SideDirWalkways++;
			}
		}

		MarkWalkwayInDirection(StartIndex, FMath::Abs(Increment) == 1, Increment);

	}

}


void DGLibraryFiller::BuildMainDirWalkways()
{
	

	int CurrIndex = Room->GetGridStartIndex();
	int GridWidth = Gen->GetTileGridWidth();
	int JumpMultiplier = bIsMainDirHorizontal ? 1 : GridWidth;

	if (Gen->GetCurrentStream()->RandRange(0, 1))
	{
		// probably not the most intuitive but it works as intended
		CurrIndex += JumpMultiplier;
	}
		

	while (true)
	{
		MarkWalkwayInDirection(CurrIndex, !bIsMainDirHorizontal);

		int Jump = Gen->GetCurrentStream()->RandRange(2, 3);
		CurrIndex += Jump * JumpMultiplier;
			
		if (!IsIndexInRoom(CurrIndex))
		{
			if (Jump > 2)
			{
				MarkWalkwayInDirection(Room->GetGridStartIndex() + (bIsMainDirHorizontal ? (Room->Width - 1) : ((Room->Height - 1) * GridWidth)), !bIsMainDirHorizontal);
			}
			break;
		}
	}
}


void DGLibraryFiller::BuildSideDirWalkways()
{
	int WalkwaysNum = FMath::Min(1 + (bIsMainDirHorizontal ? Room->Height : Room->Width) * Gen->GetCurrentStream()->RandRange(LibraryStruct->MinWalkwayRatio, LibraryStruct->MaxWalkwayRatio), (bIsMainDirHorizontal ? Room->Height : Room->Width));
	int CurrIndex = Room->GetGridStartIndex();
	int GridWidth = Gen->GetTileGridWidth();
	int JumpMultiplier = bIsMainDirHorizontal ? GridWidth : 1;


	for (int c = SideDirWalkways; c < WalkwaysNum; c++) 
	{
		int check = 0;
		bool bValid;
		do {
			check+=1;

			if (bIsMainDirHorizontal)
			{
				CurrIndex = Room->GetGridStartIndex() + Gen->GetCurrentStream()->RandRange(0, Room->Height - 1) * GridWidth;
			}
			else
			{
				CurrIndex = Room->GetGridStartIndex() + Gen->GetCurrentStream()->RandRange(0, Room->Width - 1);
			}

			bValid = true;
			DGCell Cell = (*Gen->GetTileGrid())[CurrIndex];
			if (bIsMainDirHorizontal && Cell.Role == ECellRoomRole::HorizontalWalkway) 
			{
				bValid = false;
			}
			else if (!bIsMainDirHorizontal && Cell.Role == ECellRoomRole::VerticalWalkway)
			{
				bValid = false;
			}
			
			if (check > 10)
			{
				break;
			}

		} while (!bValid);

		MarkWalkwayInDirection(CurrIndex, bIsMainDirHorizontal);
	}
}


void DGLibraryFiller::PlaceLibraries()
{
	TArray<bool> FlipAtRowOrCol;
	
	for (int i = 0; i < (bIsMainDirHorizontal ? Room->Width : Room->Height); i++)
	{
		FlipAtRowOrCol.Add((bool)Gen->GetCurrentStream()->RandRange(0, 1));
	}

	for (int c = 0; c < Room->RoomTiles.Num(); c++)
	{
		if (Room->RoomTiles[c]->Role != ECellRoomRole::Free)
		{
			continue;
		}

		int SizeX;
		int SizeY;

		if (bIsMainDirHorizontal)
		{
			SizeX = LibraryStruct->LibraryTileSizeY;
			SizeY = LibraryStruct->LibraryTileSizeX;
		}
		else
		{
			SizeX = LibraryStruct->LibraryTileSizeX;
			SizeY = LibraryStruct->LibraryTileSizeY;
		}

		TArray<int> OccupiedCells;

		if (IsEnoughPlaceForObject(c, SizeX, SizeY, OccupiedCells))
		{
			if (Gen->GetCurrentStream()->RandRange(0, 99) > LibraryStruct->ChanceToSpawnLibrary)
			{
				c += SizeY - 1;
				continue;
			}

			float MyScale = GetSizeForLibrary();
			
			FRotator LibraryRotator = GetRotationForLibrary(c, FlipAtRowOrCol);
			FVector LibraryPosition = GetPositionForLibrary(LibraryRotator, c, MyScale);

			for (int Cell : OccupiedCells)
			{
				Room->RoomTiles[Cell]->Role = ECellRoomRole::Furniture;
			}

			LibraryRotator += FRotator(0, Gen->GetCurrentStream()->FRandRange(-LibraryStruct->MaxLibraryRotation,LibraryStruct->MaxLibraryRotation), 0);

			FTransform ActorTransf;
			ActorTransf.SetLocation(LibraryPosition);
			ActorTransf.SetRotation(LibraryRotator.Quaternion());

			AActor* TempLib = Room->GetWorld()->SpawnActor<AActor>(LibraryStruct->Library, ActorTransf);
			TempLib->Tags.Add(ADGGenerator::DUNGEON_TAG);
			TempLib->SetActorScale3D(FVector(MyScale));

		}
	}

	
}


FRotator DGLibraryFiller::GetRotationForLibrary(int CellStart, TArray<bool> FlipAtRowOrColPredefined)
{
	FRotator Result = FRotator::ZeroRotator;
	if (bIsMainDirHorizontal)
	{
		Result += FRotator(0, -90.f, 0);

		if (IsValidCellAndWalkway(CellStart - Room->Height) && !IsValidCellAndWalkway(CellStart + Room->Height))
		{
			Result += FRotator(0,180.f,0);
		}
		
		else if (IsValidCellAndWalkway(CellStart - Room->Height) && IsValidCellAndWalkway(CellStart + Room->Height))
		{
			int Idx = bIsMainDirHorizontal ? GetCellCol(CellStart) : GetCellRow(CellStart);
			if (FlipAtRowOrColPredefined[Idx])
			{
				Result -= FRotator(0, 180.f, 0);
			}
		}
	}
	else
	{
		// if there is walkway above and not walkway bellow, rotate
		if (IsSameCol(CellStart - 1, CellStart) && IsValidCellAndWalkway(CellStart - 1) && (!IsSameCol(CellStart + 1, CellStart) || !IsValidCellAndWalkway(CellStart + 1)))
		{
			Result += FRotator(0, 180.f, 0);
		}
		else if (IsSameCol(CellStart - 1, CellStart) && IsSameCol(CellStart + 1, CellStart))
		{			
			// if there is walkway above and walkway bellow, rotate if the list says so
			if (IsValidCellAndWalkway(CellStart - 1) && IsValidCellAndWalkway(CellStart + 1))
			{
				int Idx = bIsMainDirHorizontal ? (GetCellCol(CellStart)) : (GetCellRow(CellStart));
				if (FlipAtRowOrColPredefined[Idx])
				{
					Result -= FRotator(0, 180.f, 0);
				}
			}
		}
		
	}

	return Result;
}


FVector DGLibraryFiller::GetPositionForLibrary(FRotator Rotation, int BaseCell, float Scale)
{
	FVector Result = Room->RoomTiles[BaseCell]->Position;

	float const ToMoveY = ((1 - Scale) * (FloorSize * LibraryStruct->LibraryTileSizeY)) / 2;
	float const ToMoveX = ((1 - Scale) * (FloorSize * LibraryStruct->LibraryTileSizeX)) / 2;

	if (bIsMainDirHorizontal)
	{		
		if (FMath::IsNearlyEqual(Rotation.Yaw, -90.f))
		{
			Result += FVector(0, FloorSize * LibraryStruct->LibraryTileSizeX, 0);
			Result.X += ToMoveY;
			Result.Y -= ToMoveX;
		}
		else
		{
			Result += FVector(FloorSize * LibraryStruct->LibraryTileSizeY, 0, 0);
			Result.X -= ToMoveY;
			Result.Y += ToMoveX;
		}	
	}
	else
	{
		if (FMath::IsNearlyEqual(FMath::Abs(Rotation.Yaw), 180.f))
		{
			Result += FVector(FloorSize * LibraryStruct->LibraryTileSizeX, FloorSize * LibraryStruct->LibraryTileSizeY, 0);
			Result.X -= ToMoveX;
			Result.Y -= ToMoveY;
		}
		else
		{
			Result.X += ToMoveX;
			Result.Y += ToMoveY;
		}
	}

	return Result;
}


float DGLibraryFiller::GetSizeForLibrary()
{
	return Gen->GetCurrentStream()->FRandRange(LibraryStruct->MinLibraryScale, LibraryStruct->MaxLibraryScale);
}


void DGLibraryFiller::PlaceLamps()
{

	TArray<DGCell*> FreeCells;
	TArray<DGCell*> ViableCells;

	for (DGCell* C : Room->RoomTiles)
	{
		if (C->Role == ECellRoomRole::Free)
		{
			FreeCells.Add(C);
		}
		else if (C->IsWalkway())
		{
			if (IsCellViableForLamp(C->RoomIndex))
			{
				ViableCells.Add(C);
			}
		}
		
	}
	

	FreeCells.Sort([this](DGCell A, DGCell B) { 
		return (bool)Room->Gen->GetCurrentStream()->RandRange(0, 1); 
		});
	

	ViableCells.Sort([this](DGCell A, DGCell B) {
		return (bool)Room->Gen->GetCurrentStream()->RandRange(0, 1); 
		});

		

	for (DGCell* f : FreeCells)
	{
		if (CurrentLamps >= MaxLamps)
		{
			break;
		}

		PlaceLamp(f->RoomIndex);
	}

	for (DGCell* f : ViableCells)
	{
		if (CurrentLamps >= MaxLamps)
		{
			break;
		}

		if (IsCellViableForLamp(f->RoomIndex))
		{
			PlaceLamp(f->RoomIndex);
		}
	}
}


void DGLibraryFiller::PlaceLamp(int Idx)
{
	if (!Room->RoomTiles.IsValidIndex(Idx))
	{
		return;
	}

	DGCell* C = Room->RoomTiles[Idx];
	C->Role = ECellRoomRole::Furniture;
	AActor* Lamp = Gen->GetWorld()->SpawnActor<AActor>(LibraryStruct->Lamp, C->Position, FRotator::ZeroRotator);
	Lamp->Tags.Add(ADGGenerator::DUNGEON_TAG);
	Lamp->SetActorScale3D(GetScaleForLamp(Lamp->GetActorScale3D()));
	FVector LampPos = C->Position + GetPositionOffsetForLamp(Lamp);
	Lamp->SetActorLocation(LampPos);

	CurrentLamps++;

}


bool DGLibraryFiller::IsCellViableForLamp(int Cell)
{
	if (Room->RoomTiles[Cell]->Role == ECellRoomRole::Entrance)
	{
		return false;
	}

	int RoomIndex = (Cell - 1) - Room->Height;

	for (int x = 0; x < Room->RoomStruct->LampSizeX + 2; x++) {

		int Col = GetCellCol(Cell - Room->Height + x * Room->Height);

		for (int y = 0; y < Room->RoomStruct->LampSizeY + 2; y++) {

			int CurrIdx = RoomIndex + x * Room->Height + y;

			if (!Room->RoomTiles.IsValidIndex(CurrIdx)
				||GetCellCol(CurrIdx) != Col
				|| Room->RoomTiles[CurrIdx]->IsWalkway()
				|| Room->RoomTiles[CurrIdx]->Role == ECellRoomRole::Free
				|| Room->RoomTiles[CurrIdx]->Role == ECellRoomRole::Entrance)
			{
				// it's more readable like this...
			}
			else
			{
				return false;
			}

		}

	}

	return true;
}
